package com.whiteibex.resource;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class ResourceGenerator {
  public static void main(String[] args) throws Exception {
    if (args.length < 1) {
      System.err.println("No resource directory provided");
      return;
    }
    if (args.length < 2) {
      exitWithError("No source destination directory provided");
    }
    if (args.length < 3) {
      exitWithError("No source destination directory provided");
    }

    File resourceDir = getDirectoryExitOnError(args[0]);
    File destinationDirectory = getDirectoryExitOnError(args[1]);
    String className = args[2];
    if (!className.contains(".")) {
      exitWithError("Cannot create class in default package");
    }

    String excludePattern = null;
    if (args.length > 3) {
      excludePattern = args[3];
    }

    File destinationFile = new File(destinationDirectory, className.replace('.', '/') + ".java");
    destinationFile.getParentFile().mkdirs();

    CharSequence content = new ResourceGenerator(resourceDir, className, excludePattern).generateResourceClass();
    FileWriter writer = new FileWriter(destinationFile);
    writer.append(content);
    writer.close();
  }

  private static File getDirectoryExitOnError(String pathname) {
    File resourceDir = new File(pathname);
    if (!resourceDir.exists()) {
      exitWithError("Directory does not exist: " + resourceDir.getAbsolutePath());
    }
    if (!resourceDir.isDirectory()) {
      exitWithError("Given directory path does not point to a directory: " + resourceDir.getAbsolutePath());
    }
    return resourceDir;
  }

  private static void exitWithError(String message) {
    System.err.println(message);
    System.exit(1);
  }

  private final File resourceDir;
  private final String className;
  private final String excludePattern;

  private ResourceGenerator(File resourceDir, String className, String excludePattern) {
    this.resourceDir = resourceDir;
    this.className = className;
    this.excludePattern = excludePattern;
  }

  private CharSequence generateResourceClass() throws IOException {
    StringBuilder builder = new StringBuilder();
    builder.append("package ");
    builder.append(className.substring(0, className.lastIndexOf('.')));
    builder.append(";");
    builder.append(System.lineSeparator());
    builder.append(System.lineSeparator());
    builder.append("public class ");
    builder.append(className.substring(1 + className.lastIndexOf('.')));
    builder.append(" {");
    builder.append(System.lineSeparator());
    processDirectory(resourceDir, builder, 1);
    builder.append("}");
    builder.append(System.lineSeparator());
    return builder;
  }

  private void processDirectory(File directory, StringBuilder builder, int level) throws IOException {
    File[] files = directory.listFiles();
    Arrays.sort(files);

    String path = directory.getPath().substring(resourceDir.getPath().length());
    if (path.startsWith("/")) {
      path = path.substring(1);
    }

    char[] array = new char[level * 2];
    Arrays.fill(array, ' ');
    String indent = String.valueOf(array);

    for (File file : files) {
      if (file.isDirectory()) {
        builder.append(System.lineSeparator());
        builder.append(indent);
        builder.append("public static class ");
        builder.append(toJavaIdentifier(file.getName()));
        builder.append(" {");
        builder.append(System.lineSeparator());
        processDirectory(file, builder, level + 1);
        builder.append(indent);
        builder.append("}");
        builder.append(System.lineSeparator());
      } else if (excludePattern == null || !file.getName().matches(excludePattern)) {
        builder.append(System.lineSeparator());
        builder.append(indent);
        builder.append("public static final String ");
        builder.append(toJavaIdentifier(file.getName()));
        builder.append(" = \"");
        builder.append(path);
        builder.append('/');
        builder.append(file.getName());
        builder.append("\";");
        builder.append(System.lineSeparator());
      }
    }
  }

  private static String toJavaIdentifier(String s) {
    char[] charArray = s.toCharArray();
    for (int i = 0; i < charArray.length; i++) {
      char c = charArray[i];
      if ((i == 0 && !Character.isJavaIdentifierStart(c))
          || !Character.isJavaIdentifierPart(c)) {
        charArray[i] = '_';
      }
    }
    return String.valueOf(charArray);
  }
}
