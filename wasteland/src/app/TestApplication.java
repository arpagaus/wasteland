/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.LoopMode;
import com.jme3.app.SimpleApplication;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitorAdapter;
import com.jme3.scene.Spatial;

public class TestApplication extends SimpleApplication {

  public static void main(String[] args) {
    new TestApplication().start();
  }

  @Override
  public void simpleInitApp() {
    flyCam.setMoveSpeed(5);

    Spatial scene = assetManager.loadModel("Models/test-parenting.blend");
    rootNode.attachChild(scene);

    rootNode.depthFirstTraversal(new SceneGraphVisitorAdapter() {
      @Override
      public void visit(Node geom) {
        AnimControl control = geom.getControl(AnimControl.class);
        if (control != null) {
          AnimChannel channel = control.createChannel();
          channel.setAnim(control.getAnimationNames().iterator().next());
          channel.setLoopMode(LoopMode.Cycle);
        }
      }

      @Override
      public void visit(Geometry geom) {
      }
    });
  }
}
