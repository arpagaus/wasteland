/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import com.brsanthu.googleanalytics.DefaultRequest;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeSystem;
import com.jme3.system.Natives;

public class Launcher {

  private final static Logger LOG = LoggerFactory.getLogger(Launcher.class);

  public static final String PREF_PREFIX = "/com/white-ibex/wasteland/";
  public static final String SETTINGS_PREF_KEY = PREF_PREFIX + "settings";

  public static final String APP_VERSION = "1.0.1";

  public static boolean isDevelopmentMode() {
    return Boolean.parseBoolean(System.getProperty("development.mode", "" + false));
  }

  public static void main(String[] args) throws Exception {
    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();

    WastelandApplication application = new WastelandApplication();
    application.setPauseOnLostFocus(false); // Otherwise breaks animations
    application.setShowSettings(false);

    AppSettings settings = getAppSettings();
    LOG.debug("Using settings: {}", settings);
    application.setSettings(settings);
    application.start();

    Analytics.postAsync(new DefaultRequest().sessionControl("start"));
  }

  private static AppSettings getAppSettings() throws BackingStoreException, IOException, LWJGLException {
    AppSettings settings = new AppSettings(true);

    Natives.extractNativeLibs(JmeSystem.getPlatform(), settings);

    settings.setTitle("Waste Land");
    if (Preferences.userRoot().node(SETTINGS_PREF_KEY).keys().length > 0) {
      settings.load(SETTINGS_PREF_KEY);
    }
    else {
      loadDefaultSettings(settings);
    }

    if (!isDisplayModeAvailable(settings)) {
      LOG.warn("Configured display mode is no longer available: {}", settings);
      loadDefaultSettings(settings);
    }
    return settings;
  }

  private static boolean isDisplayModeAvailable(AppSettings settings) throws LWJGLException {
    List<DisplayMode> displayModes = Arrays.asList(Display.getAvailableDisplayModes());
    try {
      Constructor<DisplayMode> constructor = DisplayMode.class.getDeclaredConstructor(int.class, int.class, int.class, int.class, boolean.class);
      constructor.setAccessible(true);
      DisplayMode settingsDisplayMode = constructor.newInstance(settings.getWidth(), settings.getHeight(), settings.getBitsPerPixel(), settings.getFrequency(), settings.isFullscreen());
      return displayModes.contains(settingsDisplayMode);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
      LOG.error("Could not create DisplayMode", e);
    }
    return false;
  }

  private static void loadDefaultSettings(AppSettings settings) throws LWJGLException {
    DisplayMode displayMode = Display.getDisplayMode();
    settings.setResolution(displayMode.getWidth(), displayMode.getHeight());
    settings.setFrequency(displayMode.getFrequency());
    settings.setBitsPerPixel(displayMode.getBitsPerPixel());
    settings.setSamples(4);
    settings.setFullscreen(displayMode.isFullscreenCapable());
  }
}
