package app.control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.WastelandApplication;

import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioSource.Status;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

public class DesertWindAudioControl extends AbstractControl {
  private static final Logger LOG = LoggerFactory.getLogger(DesertWindAudioControl.class);

  @Override
  public void setSpatial(Spatial spatial) {
    if (!(spatial instanceof AudioNode)) {
      throw new IllegalArgumentException("Can only be used in conjunction with " + AudioNode.class.getSimpleName());
    }
    super.setSpatial(spatial);
  }

  private AudioNode getAudioNode() {
    return (AudioNode) getSpatial();
  }

  @Override
  protected void controlRender(RenderManager arg0, ViewPort arg1) {
    if (getAudioNode().getStatus() != Status.Playing) {
      getAudioNode().play();
    }
  }

  @Override
  protected void controlUpdate(float arg0) {
    Vector3f cameraLocation = WastelandApplication.getApplication().getCameraNode().getWorldTranslation();
    float distanceSquared = getSpatial().getWorldTranslation().distanceSquared(cameraLocation);
    LOG.debug("distanceSquared: {}", distanceSquared);
    float volume = FastMath.clamp(1f / 120f * distanceSquared, 0.05f, 1f);
    getAudioNode().setVolume(volume);
  }
}
