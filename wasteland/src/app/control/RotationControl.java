/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.control;

import java.io.IOException;

import app.FastMathEx;

import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.Quaternion;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

public class RotationControl extends AbstractControl {

  private float duration;
  private Quaternion endRotation;
  private Quaternion beginRotation;

  private float time = 0f;

  public RotationControl(Quaternion beginRotation, Quaternion endRotation, int speed) {
    this(beginRotation, endRotation, FastMathEx.rotationDistance(beginRotation, endRotation) * speed);
  }

  public RotationControl(Quaternion beginRotation, Quaternion endRotation, float duration) {
    this.beginRotation = beginRotation;
    this.endRotation = endRotation;
    this.duration = duration;
  }

  public float getDuration() {
    return duration;
  }

  public Quaternion getEndRotation() {
    return endRotation;
  }

  public Quaternion getBeginRotation() {
    return beginRotation;
  }

  public float getTime() {
    return time;
  }

  @Override
  protected void controlUpdate(float tpf) {
    time = time + tpf;

    if (time > duration) {
      getSpatial().setLocalRotation(endRotation);
      getSpatial().removeControl(this);
      return;
    }

    getSpatial().getLocalRotation().slerp(beginRotation, endRotation, time / duration);
    getSpatial().forceRefresh(true, false, false);
  }

  @Override
  protected void controlRender(RenderManager rm, ViewPort vp) {
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    super.write(ex);
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.write(duration, "duration", 0);
    capsule.write(endRotation, "endRotation", null);
    capsule.write(beginRotation, "beginRotation", null);
    capsule.write(time, "time", 0);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    super.read(im);
    InputCapsule capsule = im.getCapsule(this);
    duration = capsule.readFloat("duration", 0);
    endRotation = (Quaternion) capsule.readSavable("endRotation", null);
    beginRotation = (Quaternion) capsule.readSavable("beginRotation", null);
    time = capsule.readFloat("time", 0);
  }
}
