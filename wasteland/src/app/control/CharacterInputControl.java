/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.control;

import app.WastelandApplication;
import app.appstate.InputListenerAppState;

import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

public class CharacterInputControl extends AbstractControl {
  private static final float MIN_CHARACTER_SPEED = 2.5f;
  private static final float MAX_CHARACTER_SPEED = 5f;

  public CharacterInputControl() {
  }

  private Vector3f walkDirection = new Vector3f();
  private float movingTime;

  @Override
  protected void controlUpdate(float tpf) {
    if (!isEnabled()) {
      return;
    }

    InputListenerAppState inputListener = WastelandApplication.getApplication().getStateManager().getState(InputListenerAppState.class);
    Camera camera = WastelandApplication.getApplication().getCamera();

    if (inputListener.isMoving()) {
      movingTime = movingTime + tpf;
    }
    else {
      movingTime = 0;
    }

    BetterCharacterControl characterControl = getSpatial().getControl(BetterCharacterControl.class);
    if (characterControl != null) {
      float characterSpeed = FastMath.clamp(movingTime, MIN_CHARACTER_SPEED, MAX_CHARACTER_SPEED);
      characterSpeed = characterSpeed * (1f + tpf); // Normalize

      walkDirection.zero();
      if (inputListener.isForward()) {
        walkDirection.addLocal(camera.getDirection());
      }
      if (inputListener.isBackward()) {
        walkDirection.addLocal(camera.getDirection().negateLocal());
      }
      if (inputListener.isLeft()) {
        walkDirection.addLocal(camera.getLeft());
      }
      if (inputListener.isRight()) {
        walkDirection.addLocal(camera.getLeft().negateLocal());
      }
      walkDirection.setY(0).normalizeLocal().multLocal(characterSpeed);
      characterControl.setWalkDirection(walkDirection);
    }
  }

  @Override
  protected void controlRender(RenderManager rm, ViewPort vp) {
  }
}
