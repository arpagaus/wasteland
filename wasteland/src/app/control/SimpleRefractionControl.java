package app.control;

import java.io.IOException;

import app.WastelandApplication;
import app.appstate.WastelandAppState;
import app.enumeration.UserData;

import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.scene.control.AbstractControl;

public class SimpleRefractionControl extends AbstractControl {

  private Geometry baseGeometry;
  private Geometry geometry;
  private boolean showRefractionQuad;

  public Geometry getBaseGeometry() {
    return baseGeometry;
  }

  public void setBaseGeometry(Geometry baseGeometry) {
    this.baseGeometry = baseGeometry;
  }

  public boolean isShowRefractionQuad() {
    return showRefractionQuad;
  }

  public void setShowRefractionQuad(boolean showRefractionQuad) {
    this.showRefractionQuad = showRefractionQuad;
  }

  @Override
  protected void controlUpdate(float arg0) {
    if (isShowRefractionQuad()) {
      if (geometry == null) {
        WastelandAppState appState = WastelandApplication.getAppState(WastelandAppState.class);

        geometry = new Geometry(baseGeometry.getName() + "Refraction", baseGeometry.getMesh());
        appState.getBaseNode().attachChild(geometry);
        geometry.setLocalTransform(baseGeometry.getWorldTransform());

        geometry.setMaterial(appState.getRefractionProcessor().getMaterial());
        geometry.setUserData(UserData.SELECTION_LISTENER.name(), baseGeometry.getUserData(UserData.SELECTION_LISTENER.name()));
      } else {
        geometry.setCullHint(CullHint.Inherit);
      }
    } else {
      if (geometry != null) {
        geometry.setCullHint(CullHint.Always);
      }
    }
  }

  @Override
  protected void controlRender(RenderManager arg0, ViewPort arg1) {
    // NOP
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    super.write(ex);
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.write(baseGeometry, "baseGeometry", null);
    capsule.write(showRefractionQuad, "showRefractionQuad", false);
    // don't write the geometry because it'll be lazy initialized
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    super.read(im);
    InputCapsule capsule = im.getCapsule(this);
    baseGeometry = (Geometry) capsule.readSavable("baseGeometry", null);
    showRefractionQuad = capsule.readBoolean("showRefractionQuad", false);
  }
}
