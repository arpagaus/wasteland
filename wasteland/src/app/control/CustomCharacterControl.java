package app.control;

import java.util.List;

import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsRayTestResult;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.Vector3f;
import com.jme3.util.TempVars;

/**
 * Custom character control which dampens upward velocity if the character is 0.15 or closer to the ground
 */
public class CustomCharacterControl extends BetterCharacterControl {

  public CustomCharacterControl(float radius, float height, float mass) {
    super(radius, height, mass);
    rigidBody.setFriction(0);
  }

  public void prePhysicsTick(PhysicsSpace space, float tpf) {
    checkOnGround();

    TempVars vars = TempVars.get();

    // dampen existing x/z forces
    float existingLeftVelocity = velocity.dot(localLeft);
    float existingForwardVelocity = velocity.dot(localForward);
    float existingUpVelocity = velocity.dot(localUp);

    Vector3f counterVelocity = vars.vect1;

    existingLeftVelocity = existingLeftVelocity * physicsDamping;
    existingForwardVelocity = existingForwardVelocity * physicsDamping;

    // slightly dampen upward velocity if we are "onGround"
    existingUpVelocity = existingUpVelocity * (onGround && existingUpVelocity < 0 ? .2f : 0);

    counterVelocity.set(-existingLeftVelocity, -existingUpVelocity, -existingForwardVelocity);

    localForwardRotation.multLocal(counterVelocity);
    velocity.addLocal(counterVelocity);

    float designatedVelocity = walkDirection.length();
    if (designatedVelocity > 0) {
      Vector3f localWalkDirection = vars.vect1;
      localWalkDirection.set(walkDirection).normalizeLocal();
      float existingVelocity = velocity.dot(localWalkDirection);
      float finalVelocity = designatedVelocity - existingVelocity;
      localWalkDirection.multLocal(finalVelocity);
      velocity.addLocal(localWalkDirection);
    }
    rigidBody.setLinearVelocity(velocity);

    vars.release();
  }

  protected void checkOnGround() {
    TempVars vars = TempVars.get();
    Vector3f location = vars.vect1;
    Vector3f rayVector = vars.vect2;
    float height = getFinalHeight();
    location.set(localUp).multLocal(height).addLocal(this.location);
    rayVector.set(localUp).multLocal(-height - 0.15f).addLocal(location);

    @SuppressWarnings("unchecked")
    List<PhysicsRayTestResult> results = space.rayTest(location, rayVector);
    vars.release();
    for (PhysicsRayTestResult physicsRayTestResult : results) {
      if (!physicsRayTestResult.getCollisionObject().equals(rigidBody)) {
        onGround = true;
        return;
      }
    }
    onGround = false;
  }

}