/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.control;

import com.jme3.cinematic.events.MotionEvent;
import com.jme3.math.Quaternion;

public class MotionRotationEvent extends MotionEvent {

  private Quaternion beginRotation;
  private Quaternion endRotation;

  public MotionRotationEvent() {
    super();
  }

  public Quaternion getBeginRotation() {
    return beginRotation;
  }

  public void setBeginRotation(Quaternion beginRotation) {
    this.beginRotation = beginRotation;
  }

  public Quaternion getEndRotation() {
    return endRotation;
  }

  public void setEndRotation(Quaternion endRotation) {
    this.endRotation = endRotation;
  }

  @Override
  public void onPlay() {
    // Adjust the animation duration according to the distance
    setInitialDuration(getPath().getLength());
    super.onPlay();
  }

  @Override
  public void onUpdate(float tpf) {
    super.onUpdate(tpf);
    getSpatial().getLocalRotation().slerp(
        beginRotation,
        endRotation,
        1f / getPath().getLength() * this.traveledDistance);
  }
}
