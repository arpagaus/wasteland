/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app;

import java.io.IOException;

import app.enumeration.CollisionShapeType;

import com.jme3.animation.LoopMode;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.export.Savable;

public class SpatialState implements Savable {

  private CollisionShapeType collisionShapeType;
  private LoopMode animationLoopMode;
  private boolean animationEnd;

  public CollisionShapeType getCollisionShapeType() {
    return collisionShapeType;
  }

  public SpatialState setCollisionShapeType(CollisionShapeType collisionShapeType) {
    this.collisionShapeType = collisionShapeType;
    return this;
  }

  public LoopMode getAnimationLoopMode() {
    return animationLoopMode;
  }

  public SpatialState setAnimationLoopMode(LoopMode animationLoopMode) {
    this.animationLoopMode = animationLoopMode;
    return this;
  }

  public boolean isAnimationEnd() {
    return animationEnd;
  }

  public SpatialState setAnimationEnd(boolean animationEnd) {
    this.animationEnd = animationEnd;
    return this;
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.write(collisionShapeType, "collisionShapeType", null);
    capsule.write(animationLoopMode, "animationLoopMode", null);
    capsule.write(animationEnd, "animationEnd", false);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    InputCapsule capsule = im.getCapsule(this);
    collisionShapeType = capsule.readEnum("collisionShapeType", CollisionShapeType.class, null);
    animationLoopMode = capsule.readEnum("animationLoopMode", LoopMode.class, null);
    animationEnd = capsule.readBoolean("animationEnd", false);
  }
}
