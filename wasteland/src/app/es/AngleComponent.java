/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.es;

import java.io.IOException;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;

public class AngleComponent implements Component {

  /**
   * The angle in radians
   */
  private float angle;

  public float getAngle() {
    return angle;
  }

  public void setAngle(float rotation) {
    this.angle = rotation;
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    ex.getCapsule(this).write(angle, "angle", 0);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    angle = im.getCapsule(this).readFloat("angle", 0);
  }
}
