/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.es;

import java.io.IOException;

import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.export.Savable;

public class PowerComponent implements Component, Savable {

  public enum PowerState {
    ON, OFF
  };

  private PowerState powerState = PowerState.OFF;

  public PowerState getPowerState() {
    return powerState;
  }

  public void setPowerState(PowerState powerState) {
    if (powerState == null) {
      throw new NullPointerException();
    }
    this.powerState = powerState;
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.write(powerState, "powerState", PowerState.OFF);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    InputCapsule capsule = im.getCapsule(this);
    powerState = capsule.readEnum("powerState", PowerState.class, PowerState.OFF);
  }
}
