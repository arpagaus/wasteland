/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.es;

import java.io.IOException;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;

public class PowerConsumerComponent implements Component {

  private PowerComponent powerComponent;

  public PowerComponent getPowerComponent() {
    return powerComponent;
  }

  public void setPowerComponent(PowerComponent powerComponent) {
    this.powerComponent = powerComponent;
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    ex.getCapsule(this).write(powerComponent, "powerComponent", null);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    powerComponent = (PowerComponent) im.getCapsule(this).readSavable("powerComponent", null);
  }
}
