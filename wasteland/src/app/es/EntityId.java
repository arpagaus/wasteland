/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.es;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.Savable;

public class EntityId implements Savable {

  private static final AtomicInteger SEQUENCE = new AtomicInteger(1);

  private int internalId;

  private EntityId() {
    internalId = SEQUENCE.getAndIncrement();
  }

  public static EntityId nextId() {
    return new EntityId();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + internalId;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    EntityId other = (EntityId) obj;
    if (internalId != other.internalId)
      return false;
    return true;
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    ex.getCapsule(this).write(internalId, "internalId", 0);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    internalId = im.getCapsule(this).readInt("internalId", 0);
  }
}
