/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.es;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Entity {

  private EntityId id;
  private final Map<Class<? extends Component>, Component> components = new ConcurrentHashMap<Class<? extends Component>, Component>();

  public Entity() {
    this(EntityId.nextId());
  }

  public Entity(EntityId id) {
    if (id == null) {
      throw new NullPointerException("id may not be null");
    }
    this.id = id;

    EntityStore.INSTANCE.addEntity(this);
  }

  public EntityId getId() {
    return id;
  }

  public boolean hasComponent(Class<? extends Component> componentClass) {
    return components.containsKey(componentClass);
  }

  @SuppressWarnings("unchecked")
  public <T extends Component> T getComponent(Class<T> componentClass) {
    return (T) components.get(componentClass);
  }

  public void setComponent(Component component) {
    components.put(component.getClass(), component);
  }

  public void removeComponent(Class<? extends Component> componentClass) {
    components.remove(componentClass.getClass());
  }
}
