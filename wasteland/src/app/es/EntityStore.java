/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.es;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EntityStore {

  public static final EntityStore INSTANCE = new EntityStore();

  private final Map<EntityId, Entity> entities = new ConcurrentHashMap<EntityId, Entity>();

  private EntityStore() {
  }

  void addEntity(Entity entity) {
    entities.put(entity.getId(), entity);
  }

  public Entity findEntity(EntityId id) {
    return entities.get(id);
  }
}
