/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.appstate.CharacterSelectionAppState;
import app.appstate.InputListenerAppState;
import app.appstate.MenuAppState;
import app.control.CharacterInputControl;
import app.control.CustomCharacterControl;

import com.brsanthu.googleanalytics.DefaultRequest;
import com.brsanthu.googleanalytics.ExceptionHit;
import com.jme3.app.DebugKeysAppState;
import com.jme3.app.SimpleApplication;
import com.jme3.app.StatsAppState;
import com.jme3.app.state.AppState;
import com.jme3.app.state.ScreenshotAppState;
import com.jme3.audio.Environment;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.BloomFilter;
import com.jme3.post.filters.BloomFilter.GlowMode;
import com.jme3.post.filters.FadeFilter;
import com.jme3.post.filters.FogFilter;
import com.jme3.scene.Node;
import com.jme3.scene.control.CameraControl;
import com.jme3.scene.control.CameraControl.ControlDirection;

public class WastelandApplication extends SimpleApplication {

  private static final Logger LOG = LoggerFactory.getLogger(WastelandApplication.class);

  public static final String SCREENSHOT_FILE_NAME = "screenshot";
  public static final String ZIP_ENTRY_SCENE = "scene";

  public static final Vector3f DEFAULT_TRANSLATION = new Vector3f(0, 1.65f, 0);

  private static WastelandApplication INSTANCE;

  public static WastelandApplication getApplication() {
    if (INSTANCE == null) {
      throw new NullPointerException("Application is not initialized yet");
    }
    return INSTANCE;
  }

  public static <T extends AppState> T getAppState(Class<T> clazz) {
    return getApplication().stateManager.getState(clazz);
  }

  private BulletAppState bulletAppState;

  private Node characterNode;
  private Node cameraNode;
  private FilterPostProcessor postProcessor;

  public WastelandApplication() {
    super(
        new StatsAppState(),
        new InputListenerAppState(),
        new DebugKeysAppState(),
        new CharacterSelectionAppState(),
        new MenuAppState());
  }

  public Node getCharacterNode() {
    return characterNode;
  }

  public Node getCameraNode() {
    return cameraNode;
  }

  public FilterPostProcessor getPostProcessor() {
    return postProcessor;
  }

  @Override
  public void simpleInitApp() {
    INSTANCE = this;

    LOG.info("Renderer Caps: {}", renderer.getCaps().toString());

    if (!Launcher.isDevelopmentMode()) {
      stateManager.getState(StatsAppState.class).toggleStats();
    }

    ScreenshotAppState screenshotAppState = new ScreenshotAppState();
    stateManager.attach(screenshotAppState);
    viewPort.addProcessor(screenshotAppState);

    // Physics
    bulletAppState = new BulletAppState();
    stateManager.attach(bulletAppState);
    bulletAppState.setEnabled(false);

    // Player
    characterNode = new Node("CharacterNode");
    rootNode.attachChild(characterNode);
    characterNode.addControl(new CharacterInputControl());

    BetterCharacterControl characterControl = new CustomCharacterControl(0.4f, 1.7f, 100);
    characterControl.setPhysicsDamping(1);

    bulletAppState.getPhysicsSpace().add(characterControl);
    characterNode.addControl(characterControl);

    cameraNode = new Node("CameraNode");
    cameraNode.rotate(0, FastMath.PI, 0);
    characterNode.attachChild(cameraNode);
    cameraNode.setLocalTranslation(DEFAULT_TRANSLATION);
    cameraNode.addControl(new CameraControl(cam, ControlDirection.SpatialToCamera));

    // Set frustum near to 0.25
    cam.setFrustumPerspective(45, (float) cam.getWidth() / cam.getHeight(), .25f, 1000f);

    initPostProcessor();

    audioRenderer.setEnvironment(Environment.AcousticLab);
  }

  private void initPostProcessor() {
    postProcessor = new FilterPostProcessor(assetManager);
    postProcessor.setNumSamples(Math.max(1, getContext().getSettings().getSamples()));

    viewPort.addProcessor(postProcessor);

    BloomFilter bloomFilter = new BloomFilter(GlowMode.Objects);
    bloomFilter.setDownSamplingFactor(2);
    postProcessor.addFilter(bloomFilter);

    FogFilter fogFilter = new FogFilter();
    fogFilter.setFogColor(new ColorRGBA(1f, 0.9f, 0.6f, 1.0f));
    fogFilter.setFogDistance(100);
    fogFilter.setFogDensity(0.6f);
    postProcessor.addFilter(fogFilter);

    VignetteFilter vignetteFilter = new VignetteFilter();
    vignetteFilter.setRadius(0.9f);
    vignetteFilter.setSoftness(0.5f);
    vignetteFilter.setStrength(0.6f);
    postProcessor.addFilter(vignetteFilter);

    FadeFilter fadeFilter = new FadeFilter(3);
    postProcessor.addFilter(fadeFilter);
  }

  public void setPostProcessorEnabled(boolean enabled) {
    if (enabled) {
      viewPort.addProcessor(postProcessor);
    } else {
      viewPort.removeProcessor(postProcessor);
    }
  }

  @Override
  public void simpleUpdate(float tpf) {
    // AudioListener
    listener.setLocation(cam.getLocation());
    listener.setRotation(cam.getRotation());
  }

  @Override
  public void stop(boolean waitFor) {
    super.stop(waitFor);

    // Use a separate non-daemon thread to prevent blocking the JME stop
    new Thread("Analytics session stop") {
      @Override
      public void run() {
        LOG.info("Sending analytics session stop");
        Analytics.postSync(new DefaultRequest().sessionControl("stop"));
        LOG.info("Finished shutdown");
      }
    }.start();

    // Make sure INSTANCE is set to null in the next update()
    enqueue(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        INSTANCE = null;
        return null;
      }
    });
  }

  @Override
  public void handleError(String errMsg, Throwable t) {
    Analytics.postAsync(new ExceptionHit(getExceptionHitMessage(t), true));
    super.handleError(errMsg, t);
  }

  private String getExceptionHitMessage(final Throwable throwable) {
    Throwable cause = throwable;
    try {
      while (cause.getCause() != null) {
        cause = cause.getCause();
      }

      StackTraceElement element = cause.getStackTrace()[0];
      String message = cause.getClass().getSimpleName() + " (" + element.getFileName() + ":" + element.getLineNumber() + ")";
      return message.substring(0, Math.min(150, message.length()));
    } catch (Throwable t) {
      LOG.error("Failed to build message", t);
      return "Failed to build message";
    }
  }
}
