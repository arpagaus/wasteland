package app;

import java.io.IOException;

import com.jme3.asset.AssetManager;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.material.Material;
import com.jme3.math.Vector2f;
import com.jme3.post.Filter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;

/**
 * Originally from <a href="https://github.com/mattdesl/lwjgl-basics/wiki/ShaderLesson3">https://github.com/mattdesl/lwjgl-basics/wiki/ShaderLesson3</a>
 * 
 * @author Remo Arpagaus
 */
public class VignetteFilter extends Filter {

  private static final float DEFAULT_STRENTH = 0.5f;
  private static final float DEFAULT_SOFTNESS = 0.5f;
  private static final float DEFAULT_RADIUS = 0.75f;

  private Material material;

  private float radius = DEFAULT_RADIUS;
  private float softness = DEFAULT_SOFTNESS;
  private float strength = DEFAULT_STRENTH;

  public VignetteFilter() {
    super(VignetteFilter.class.getSimpleName());
  }

  @Override
  protected void initFilter(AssetManager assetManager, RenderManager arg1, ViewPort arg2, int w, int h) {
    material = new Material(assetManager, "MatDefs/Post/Vignette.j3md");
    material.setVector2("Resolution", new Vector2f(w, h));
    material.setFloat("Radius", radius);
    material.setFloat("Softness", softness);
    material.setFloat("Strength", strength);
  }

  @Override
  protected Material getMaterial() {
    return material;
  }

  public float getRadius() {
    return radius;
  }

  /**
   * The radius of the vignette. Where a value of 0.5 will lead to a circle fitting the screen.
   * 
   * @param radius
   */
  public void setRadius(float radius) {
    checkFloatArgument(radius, 0, 1, "radius");
    this.radius = radius;
  }

  public float getSoftness() {
    return softness;
  }

  /**
   * The softness of the vignette's edge. A value between 0 and 1.
   * 
   * @param softness
   */
  public void setSoftness(float softness) {
    checkFloatArgument(softness, 0, 1, "softness");
    this.softness = softness;
  }

  public float getStrength() {
    return strength;
  }

  /**
   * The strength vignette. A value between 0 and 1. Where a value of 0 will result in no visible effect.
   * 
   * @param strength
   */
  public void setStrength(float strength) {
    checkFloatArgument(strength, 0, 1, "strength");
    this.strength = strength;
  }

  private static void checkFloatArgument(float value, float min, float max, String name) {
    if (value < min || value > max) {
      throw new IllegalArgumentException(name + " was " + value + " but should be between " + min + " and " + max);
    }
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    super.write(ex);
    OutputCapsule oc = ex.getCapsule(this);
    oc.write(radius, "radius", DEFAULT_RADIUS);
    oc.write(softness, "softness", DEFAULT_SOFTNESS);
    oc.write(strength, "strength", DEFAULT_STRENTH);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    super.read(im);
    InputCapsule ic = im.getCapsule(this);
    radius = ic.readFloat("radius", DEFAULT_RADIUS);
    softness = ic.readFloat("softness", DEFAULT_SOFTNESS);
    strength = ic.readFloat("strength", DEFAULT_STRENTH);
  }
}
