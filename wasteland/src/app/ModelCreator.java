/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jme3.asset.AssetManager;
import com.jme3.asset.BlenderKey;
import com.jme3.asset.BlenderKey.FeaturesToLoad;
import com.jme3.export.binary.BinaryExporter;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh.Mode;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitorAdapter;
import com.jme3.scene.UserData;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.util.TangentBinormalGenerator;

public class ModelCreator {

  private static final Logger LOG = LoggerFactory.getLogger(ModelCreator.class.getName());

  // Used for bump maps
  private static final boolean GENERATE_TANGENT_BINORMAL = false;

  public static Node loadBlenderModel(String name, AssetManager assetManager) {
    String j3oName = name.replace(".blend", ".j3o");
    if (!Launcher.isDevelopmentMode()) {
      return (Node) assetManager.loadModel(j3oName);
    }

    Material defaultMaterial = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
    defaultMaterial.setBoolean("UseMaterialColors", true);
    defaultMaterial.setColor("Diffuse", ColorRGBA.DarkGray);
    defaultMaterial.setColor("Ambient", ColorRGBA.DarkGray);
    defaultMaterial.setColor("Specular", ColorRGBA.White);

    BlenderKey key = new BlenderKey(name);
    key.setDefaultMaterial(defaultMaterial);
    key.setLoadGeneratedTextures(false);
    key.setLayersToLoad(1);
    key.excludeFromLoading(
        FeaturesToLoad.LIGHTS |
            FeaturesToLoad.CAMERAS |
            FeaturesToLoad.TEXTURES |
            FeaturesToLoad.MATERIALS // without this not all UVs are loaded correctly, because meshed might be split
        );

    Node model = (Node) assetManager.loadModel(key);
    model.breadthFirstTraversal(new SceneGraphVisitorAdapter() {
      @SuppressWarnings("unused")
      @Override
      public void visit(Geometry geometry) {
        // Fix mesh data. Otherwise bullet will crash
        geometry.getMesh().updateCounts();

        if (geometry.getMesh().getMode() != Mode.Triangles) {
          LOG.info("Ignoring mesh {} for physics because it's mode is {}", geometry, geometry.getMesh().getMode());
          geometry.setUserData(UserData.JME_PHYSICSIGNORE, true);
        }

        if (GENERATE_TANGENT_BINORMAL && geometry.getMesh().getBuffer(Type.TexCoord) != null) {
          TangentBinormalGenerator.generate(geometry.getMesh());
        }
      }

    });

    try {
      BinaryExporter.getInstance().save(model, new File("assets/" + j3oName));
    } catch (IOException e) {
      LOG.error("Failed to store j3o model", e);
    }
    return model;
  }
}
