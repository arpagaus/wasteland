/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.enumeration;

import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.scene.Spatial;

public enum CollisionShapeType {
  Mesh {
    @Override
    public CollisionShape createCollisionShape(Spatial spatial) {
      return CollisionShapeFactory.createMeshShape(spatial);
    }
  },
  Box {
    @Override
    public CollisionShape createCollisionShape(Spatial spatial) {
      return CollisionShapeFactory.createBoxShape(spatial);
    }
  };

  public abstract CollisionShape createCollisionShape(Spatial spatial);
}
