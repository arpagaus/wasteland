/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.enumeration;

import app.SpatialState;
import app.es.PowerComponent;
import app.scene.SelectionListener;

public enum UserData {
  /**
   * {@link SpatialState}
   */
  SPATIAL_STATE,
  /**
   * {@link SelectionListener}
   */
  SELECTION_LISTENER,

  /**
   * {@link PowerComponent}
   */
  POWER_COMPONENT,
  /**
   * {@link PowerConsumerComp}
   */
  POWER_CONSUMER_COMPONENT,
  /**
   * {@link AngleComp}
   */
  ANGLE_COMPONENT
}
