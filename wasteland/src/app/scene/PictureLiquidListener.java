package app.scene;

import java.io.IOException;

import app.WastelandApplication;
import app.appstate.MenuAppState;
import app.appstate.WastelandAppState;

import com.google.common.base.Preconditions;
import com.jme3.app.state.AppStateManager;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.post.filters.FadeFilter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

public class PictureLiquidListener implements SelectionListener {

  private boolean enabled;
  private Spatial spatial;

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public Spatial getSpatial() {
    return spatial;
  }

  public void setSpatial(Spatial spatial) {
    Preconditions.checkNotNull(spatial);
    this.spatial = spatial;
  }

  @Override
  public void onSelection(SelectionEvent event) {
    final FadeFilter filter = WastelandApplication.getApplication().getPostProcessor().getFilter(FadeFilter.class);
    if (filter != null) {
      filter.fadeOut();
      spatial.addControl(new AbstractControl() {

        @Override
        protected void controlUpdate(float arg0) {
          if (filter.getValue() == 0) {
            endGame();
          }
        }

        @Override
        protected void controlRender(RenderManager arg0, ViewPort arg1) {
        }
      });
    }
    else {
      endGame();
    }
  }

  private void endGame() {
    AppStateManager stateManager = WastelandApplication.getApplication().getStateManager();
    WastelandAppState appState = stateManager.getState(WastelandAppState.class);
    if (appState != null) {
      stateManager.detach(appState);
    }
    MenuAppState menuAppState = stateManager.getState(MenuAppState.class);
    menuAppState.toggleVisible();
    menuAppState.showOutro();
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.write(enabled, "enabled", true);
    capsule.write(spatial, "spatial", null);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    InputCapsule capsule = im.getCapsule(this);
    enabled = capsule.readBoolean("enabled", true);
    spatial = (Spatial) capsule.readSavable("spatial", null);
  }

}
