/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import java.io.IOException;

import app.control.RotationControl;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.math.Quaternion;

public class ReflectorTiltPanelSelectionListener extends AbstractReflectorPanelSelectionListener {

  private static final float TICK_ANIM_DURATION = .75f;

  public ReflectorTiltPanelSelectionListener() {
  }

  protected void doRotation(Quaternion beginRotation, Quaternion endRotation, final int numberOfTicks) {
    float totalTime = numberOfTicks * TICK_ANIM_DURATION;
    rotationNode.addControl(new RotationControl(beginRotation, endRotation, totalTime));
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    super.write(ex);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    super.read(im);
  }
}
