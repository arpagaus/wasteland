/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.FastMathEx;
import app.control.RotationControl;
import app.enumeration.UserData;
import app.es.AngleComponent;

import com.google.common.base.Preconditions;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public abstract class AbstractReflectorPanelSelectionListener extends AttachCameraSelectionListener {
  private static final Logger LOG = LoggerFactory.getLogger(AbstractReflectorPanelSelectionListener.class.getName());

  /**
   * 22.5 degrees, a.k.a. one tick
   */
  public final static float QUARTER_PI = FastMath.HALF_PI * 0.25f;

  protected Node rotationNode;

  public AbstractReflectorPanelSelectionListener() {
  }

  public void setRotationNode(Node needleNode) {
    Preconditions.checkNotNull(needleNode);
    this.rotationNode = needleNode;
  }

  @Override
  public void onSelection(SelectionEvent event) {
    if (!isCurrentlyAttached()) {
      super.onSelection(event);
      // Usually the listener is removed but in this case we wanna keep it
      getParentSpatial().setUserData(UserData.SELECTION_LISTENER.name(), this);
    } else {
      Vector3f localSelectionTranslation = getParentSpatial().worldToLocal(event.getSelectionPoint(), null);
      LOG.info("localSelectionTranslation=" + localSelectionTranslation);

      boolean isRotationRunning = rotationNode.getControl(RotationControl.class) != null;
      if (localSelectionTranslation.length() > .15f && !isRotationRunning) {
        Vector2f coordinates = new Vector2f(localSelectionTranslation.x, localSelectionTranslation.y);
        float newAngle = FastMath.atan2(coordinates.y, coordinates.x);
        newAngle = newAngle - FastMath.HALF_PI; // Because the initial position is already rotated 90°
        newAngle = roundInSteps(newAngle, QUARTER_PI);

        AngleComponent angleComponent = rotationNode.getUserData(UserData.ANGLE_COMPONENT.name());
        angleComponent.setAngle(newAngle);

        LOG.info("Storing an angle of " + newAngle * FastMath.RAD_TO_DEG + " on " + rotationNode);

        Quaternion beginRotation = this.rotationNode.getLocalRotation().clone();
        Quaternion endRotation = new Quaternion(new float[] { 0f, 0f, newAngle });

        float rotationDistance = FastMathEx.rotationDistance(beginRotation, endRotation);
        final int numberOfTicks = getNumberOfTicks(rotationDistance);

        if (numberOfTicks > 0) {
          doRotation(beginRotation, endRotation, numberOfTicks);
        }
      }
    }
  }

  protected abstract void doRotation(Quaternion beginRotation, Quaternion endRotation, final int numberOfTicks);

  protected int getNumberOfTicks(float rotationDistance) {
    int integerRotationDistance = Math.round(rotationDistance * 100f);

    switch (integerRotationDistance) {
    case 0:
      return 0;
    case 4:
      return 1;
    case 15:
      return 2;
    case 31:
      return 3;
    case 50:
      return 4;
    case 69:
      return 5;
    case 85:
      return 6;
    case 96:
      return 7;
    case 100:
      return 8;
    default:
      LOG.error("There is no crank count available for " + integerRotationDistance);
      return 0;
    }
  }

  protected static float roundInSteps(final float angle, final float stepSize) {
    float newAngle = FastMath.abs(angle);

    float remainder = newAngle % stepSize;
    if (remainder >= (stepSize * .5f)) {
      // Roundup
      newAngle = newAngle - remainder + stepSize;
    }
    else {
      // Floor
      newAngle = newAngle - remainder;
    }

    if (angle < 0) {
      return -newAngle;
    } else {
      return newAngle;
    }
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    super.write(ex);
    ex.getCapsule(this).write(rotationNode, "rotationNode", null);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    super.read(im);
    rotationNode = (Node) im.getCapsule(this).readSavable("rotationNode", null);
  }
}
