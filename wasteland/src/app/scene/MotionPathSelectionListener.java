/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import java.io.IOException;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import app.WastelandApplication;
import app.listener.AbstractCinematicEventListener;

import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.events.CinematicEvent;
import com.jme3.cinematic.events.MotionEvent;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.Spline.SplineType;
import com.jme3.math.Vector3f;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.util.BufferUtils;

public class MotionPathSelectionListener implements SelectionListener {

  private Vector3f[] wayPoints;

  public MotionPathSelectionListener() {
  }

  public MotionPathSelectionListener(int wayPointCount) {
    this.wayPoints = new Vector3f[wayPointCount];
  }

  public void setWayPoint(Vector3f wayPoint, int index) {
    this.wayPoints[index] = wayPoint;
  }

  public void setMotionPathMesh(Mesh motionPathMesh) {
    Buffer buffer = motionPathMesh.getBuffer(Type.Position).getDataReadOnly();
    Vector3f[] verticies = BufferUtils.getVector3Array((FloatBuffer) buffer);
    System.out.println(Arrays.toString(verticies));
  }

  @Override
  public void onSelection(SelectionEvent event) {
    final Node characterNode = WastelandApplication.getApplication().getCharacterNode();

    // Return instantly if the characterNode is already moving
    if (characterNode.getControl(MotionEvent.class) != null) {
      return;
    }

    characterNode.getControl(BetterCharacterControl.class).setEnabled(false);

    MotionPath path = new MotionPath();
    path.addWayPoint(characterNode.getWorldTranslation());
    for (Vector3f wayPoint : wayPoints) {
      path.addWayPoint(wayPoint);
    }
    path.setPathSplineType(SplineType.Linear);

    MotionEvent motionEvent = new MotionEvent(characterNode, path, 5);
    motionEvent.play();
    motionEvent.addListener(new AbstractCinematicEventListener() {
      @Override
      public void onStop(CinematicEvent cinematic) {
        super.onStop(cinematic);

        characterNode.removeControl(MotionEvent.class);
        characterNode.getControl(BetterCharacterControl.class).setEnabled(true);
      }
    });
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.writeSavableArrayList(new ArrayList<>(Arrays.asList(wayPoints)), "wayPoints", null);
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  @Override
  public void read(JmeImporter im) throws IOException {
    InputCapsule capsule = im.getCapsule(this);
    ArrayList wayPoints = capsule.readSavableArrayList("wayPoints", null);
    if (wayPoints == null) {
      this.wayPoints = null;
    }
    else {
      this.wayPoints = (Vector3f[]) wayPoints.toArray(new Vector3f[wayPoints.size()]);
    }
  }
}
