/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import com.jme3.export.Savable;
import com.jme3.math.Vector3f;

public interface SelectionListener extends Savable {

  public void onSelection(SelectionEvent event);

  public class SelectionEvent {
    private Vector3f selectionPoint;

    public SelectionEvent(Vector3f selectionPoint) {
      this.selectionPoint = selectionPoint;
    }

    public Vector3f getSelectionPoint() {
      return selectionPoint;
    }
  }
}
