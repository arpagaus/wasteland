/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import java.io.IOException;

import app.WastelandApplication;
import app.appstate.InputListenerAppState;
import app.control.MotionRotationEvent;
import app.enumeration.UserData;
import app.listener.AbstractCinematicEventListener;

import com.google.common.base.Preconditions;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.events.CinematicEvent;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.Transform;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

public class AttachCameraSelectionListener implements SelectionListener {

  private boolean currentlyAttached;

  private Spatial parentSpatial;

  private Node releaseCharacterNode;
  private Transform releaseCharacterTransform;
  private Node attachCameraNode;

  public AttachCameraSelectionListener() {
  }

  public void setParentSpatial(Spatial parentSpatial) {
    Preconditions.checkNotNull(parentSpatial, "parentSpatial may not be null");
    this.parentSpatial = parentSpatial;
  }

  protected Spatial getParentSpatial() {
    return parentSpatial;
  }

  public void setAttachCameraNode(Node attachCameraNode) {
    Preconditions.checkNotNull(attachCameraNode, "attachCameraNode may not be null");
    this.attachCameraNode = attachCameraNode;
  }

  public Node getAttachCameraNode() {
    return attachCameraNode;
  }

  public void setReleaseCharacterNode(Node releaseCharacterNode) {
    Preconditions.checkNotNull(releaseCharacterNode, "releaseCharacterNode not be null");
    this.releaseCharacterNode = releaseCharacterNode;
  }

  protected boolean isCurrentlyAttached() {
    return currentlyAttached;
  }

  @Override
  public void onSelection(SelectionEvent event) {
    WastelandApplication application = WastelandApplication.getApplication();
    final Node rootNode = application.getRootNode();
    final Node characterNode = application.getCharacterNode();
    final Node cameraNode = application.getCameraNode();
    final InputListenerAppState inputListener = application.getStateManager().getState(InputListenerAppState.class);

    // Either a specific release location is specified or we are going back where came from
    if (releaseCharacterNode != null) {
      releaseCharacterTransform = releaseCharacterNode.getWorldTransform();
    }
    else {
      releaseCharacterTransform = new Transform();
      releaseCharacterTransform.setTranslation(characterNode.getWorldTranslation());
      releaseCharacterTransform.setRotation(cameraNode.getWorldRotation());
    }

    parentSpatial.setUserData(UserData.SELECTION_LISTENER.name(), null);
    characterNode.getControl(BetterCharacterControl.class).setEnabled(false);

    inputListener.setCursorVisible(true);
    inputListener.setCursorVisibilityModifiable(false);

    MotionPath path = new MotionPath();
    path.addWayPoint(cameraNode.getWorldTranslation().clone());
    path.addWayPoint(attachCameraNode.getWorldTranslation().clone());

    MotionRotationEvent motionEvent = new MotionRotationEvent();
    cameraNode.addControl(motionEvent);
    motionEvent.setPath(path);
    motionEvent.setBeginRotation(cameraNode.getWorldRotation().clone());
    motionEvent.setEndRotation(attachCameraNode.getWorldRotation().clone());
    motionEvent.play();

    rootNode.attachChild(cameraNode);
    cameraNode.setLocalTranslation(path.getWayPoint(0));

    currentlyAttached = true;

    motionEvent.addListener(new AbstractCinematicEventListener() {
      @Override
      public void onStop(CinematicEvent cinematic) {
        super.onStop(cinematic);
        cameraNode.removeControl(MotionRotationEvent.class);

        cameraNode.setLocalTransform(Transform.IDENTITY.clone());
        attachCameraNode.attachChild(cameraNode);
      }
    });

    cameraNode.addControl(new ReleaseCameraControl(this));
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.write(currentlyAttached, "currentlyAttached", false);
    capsule.write(parentSpatial, "parentSpatial", null);
    capsule.write(releaseCharacterNode, "releaseCharacterNode", null);
    capsule.write(releaseCharacterTransform, "releaseCharacterTransform", null);
    capsule.write(attachCameraNode, "attachCameraNode", null);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    InputCapsule capsule = im.getCapsule(this);
    currentlyAttached = capsule.readBoolean("currentlyAttached", false);
    parentSpatial = (Spatial) capsule.readSavable("parentSpatial", null);
    releaseCharacterNode = (Node) capsule.readSavable("releaseCharacterNode", null);
    releaseCharacterTransform = (Transform) capsule.readSavable("releaseCharacterTransform", null);
    attachCameraNode = (Node) capsule.readSavable("attachCameraNode", null);
  }

  public static final class ReleaseCameraControl extends AbstractControl {
    private AttachCameraSelectionListener parent;
    private InputListenerAppState inputListener;

    private ReleaseCameraControl(AttachCameraSelectionListener parent) {
      this();
      this.parent = parent;
    }

    public ReleaseCameraControl() {
      this.inputListener = WastelandApplication.getApplication().getStateManager().getState(InputListenerAppState.class);
    }

    @Override
    protected void controlUpdate(float tpf) {
      if (inputListener.consumeRelease()) {
        parent.currentlyAttached = false;
        getSpatial().removeControl(this);

        WastelandApplication application = WastelandApplication.getApplication();
        final Node rootNode = application.getRootNode();
        final Node cameraNode = application.getCameraNode();

        Transform srcTransform = cameraNode.getWorldTransform().clone();

        MotionPath path = new MotionPath();
        path.addWayPoint(srcTransform.getTranslation());
        path.addWayPoint(parent.releaseCharacterTransform.getTranslation().add(WastelandApplication.DEFAULT_TRANSLATION));

        MotionRotationEvent motionEvent = new MotionRotationEvent();
        cameraNode.addControl(motionEvent);
        motionEvent.setPath(path);
        motionEvent.setBeginRotation(cameraNode.getWorldRotation().clone());
        motionEvent.setEndRotation(parent.releaseCharacterTransform.getRotation().clone());
        motionEvent.play();

        rootNode.attachChild(cameraNode);
        cameraNode.setLocalTransform(srcTransform);

        motionEvent.addListener(new AbstractCinematicEventListener() {
          public void onStop(CinematicEvent cinematic) {
            cameraNode.removeControl(MotionRotationEvent.class);

            final Node characterNode = WastelandApplication.getApplication().getCharacterNode();
            rootNode.attachChild(characterNode);
            characterNode.setLocalTranslation(parent.releaseCharacterTransform.getTranslation());
            characterNode.getControl(BetterCharacterControl.class).setEnabled(true);

            characterNode.attachChild(cameraNode);
            cameraNode.setLocalTranslation(WastelandApplication.DEFAULT_TRANSLATION);

            parent.parentSpatial.setUserData(UserData.SELECTION_LISTENER.name(), parent);

            inputListener.setCursorVisible(false);
            inputListener.setCursorVisibilityModifiable(true);
          }
        });
      }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
      super.write(ex);
      ex.getCapsule(this).write(this.parent, "parent", null);
    }

    @Override
    public void read(JmeImporter im) throws IOException {
      super.read(im);
      parent = (AttachCameraSelectionListener) im.getCapsule(this).readSavable("parent", null);
    }
  }
}
