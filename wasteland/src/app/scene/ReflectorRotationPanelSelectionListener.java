/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import java.io.IOException;

import app.control.RotationControl;
import app.listener.AbstractAnimEventListener;

import com.google.common.base.Preconditions;
import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.Quaternion;
import com.jme3.scene.Node;

public class ReflectorRotationPanelSelectionListener extends AbstractReflectorPanelSelectionListener {
  private Node crankNode;

  public ReflectorRotationPanelSelectionListener() {
  }

  public void setCrankNode(Node crankNode) {
    Preconditions.checkNotNull(crankNode);
    this.crankNode = crankNode;
  }

  protected void doRotation(Quaternion beginRotation, Quaternion endRotation, final int numberOfTicks) {
    AnimControl crankAnimControl = crankNode.getControl(AnimControl.class);
    float crankAnimLength = crankAnimControl.getAnim(crankAnimControl.getAnimationNames().iterator().next()).getLength();
    float totalTime = numberOfTicks * crankAnimLength;

    rotationNode.addControl(new RotationControl(beginRotation, endRotation, totalTime));

    crankAnimControl.clearListeners();
    crankAnimControl.addListener(new AbstractAnimEventListener() {
      int crankCountdown = numberOfTicks;

      @Override
      public void onAnimCycleDone(AnimControl control, AnimChannel channel, String name) {
        super.onAnimCycleDone(control, channel, name);

        crankCountdown--;
        if (crankCountdown <= 0) {
          control.setEnabled(false);
        }
      }
    });

    crankAnimControl.setEnabled(true);
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    super.write(ex);
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.write(crankNode, "crankNode", null);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    super.read(im);
    InputCapsule capsule = im.getCapsule(this);
    crankNode = (Node) capsule.readSavable("crankNode", null);
  }
}
