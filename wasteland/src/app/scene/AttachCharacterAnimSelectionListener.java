/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.WastelandApplication;
import app.enumeration.AttachCharacterType;
import app.listener.AbstractCinematicEventListener;

import com.google.common.base.Preconditions;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.events.CinematicEvent;
import com.jme3.cinematic.events.MotionEvent;
import com.jme3.collision.CollisionResults;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public class AttachCharacterAnimSelectionListener extends AnimSelectionListener {

  private static final Logger LOG = LoggerFactory.getLogger(AttachCharacterAnimSelectionListener.class.getName());

  private Node attachNode;
  private AttachCharacterType attachCharacterType = AttachCharacterType.Always;

  public AttachCharacterAnimSelectionListener() {
  }

  public AttachCharacterAnimSelectionListener(AttachCharacterType attachCharacterType) {
    this.attachCharacterType = attachCharacterType;
  }

  public void setAttachNode(Node node) {
    Preconditions.checkNotNull(node);
    this.attachNode = node;
    addAnimationNode(node);
  }

  @Override
  public void onSelection(SelectionEvent event) {
    super.onSelection(event);

    final Node characterNode = WastelandApplication.getApplication().getCharacterNode();

    if (checkAttachCharacter(attachNode)) {
      if (attachNode == null) {
        LOG.error("There is no attachNode ");
      } else if (isAnimationEnabledForNode(attachNode) &&
          characterNode.getControl(MotionEvent.class) == null) {

        characterNode.getControl(BetterCharacterControl.class).setEnabled(false);

        MotionPath path = new MotionPath();
        path.addWayPoint(characterNode.getWorldTranslation());
        path.addWayPoint(attachNode.getWorldTranslation());
        MotionEvent motionEvent = new MotionEvent(characterNode, path, 1);
        motionEvent.play();
        motionEvent.addListener(new AbstractCinematicEventListener() {
          @Override
          public void onStop(CinematicEvent cinematic) {
            super.onStop(cinematic);

            characterNode.setLocalTranslation(Vector3f.ZERO);
            attachNode.attachChild(characterNode);
            characterNode.removeControl(MotionEvent.class);
          }
        });
      }
    }
  }

  private boolean checkAttachCharacter(Node node) {
    if (attachCharacterType == AttachCharacterType.Always) {
      return true;
    }
    else if (attachCharacterType == AttachCharacterType.IfBottomContact) {
      Node characterNode = WastelandApplication.getApplication().getCharacterNode();
      Ray ray = new Ray(characterNode.getLocalTranslation(), Vector3f.UNIT_Y.negate());
      ray.setLimit(0.2f);
      if (node.collideWith(ray, new CollisionResults()) > 0) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    super.write(ex);
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.write(attachNode, "attachNode", null);
    capsule.write(attachCharacterType, "attachCharacterType", AttachCharacterType.Always);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    super.read(im);
    InputCapsule capsule = im.getCapsule(this);
    attachNode = (Node) capsule.readSavable("attachNode", null);
    attachCharacterType = capsule.readEnum("attachCharacterType", AttachCharacterType.class, AttachCharacterType.Always);
  }
}
