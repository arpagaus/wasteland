/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.enumeration.UserData;
import app.es.PowerComponent.PowerState;
import app.es.PowerConsumerComponent;

import com.google.common.base.Preconditions;
import com.jme3.animation.AnimControl;
import com.jme3.audio.AudioNode;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.export.Savable;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class AnimSelectionListener implements SelectionListener, Savable {

  private static final Logger LOG = LoggerFactory.getLogger(AnimSelectionListener.class.getName());

  private Set<Node> animationNodes = new HashSet<Node>();

  public void addAnimationNode(Node node) {
    Preconditions.checkNotNull(node);
    animationNodes.add(node);
  }

  @Override
  public void onSelection(SelectionEvent event) {
    for (final Node node : animationNodes) {
      if (!isAnimationEnabledForNode(node)) {
        LOG.info("Will not animate {}", node);
        continue;
      }

      AnimControl control = node.getControl(AnimControl.class);
      if (control != null) {
        control.setEnabled(true);
        for (Spatial child : node.getChildren()) {
          if (child instanceof AudioNode) {
            ((AudioNode) child).play();
          }
        }
      }
      else {
        LOG.info("There is no AnimControl on {}", node);
      }
    }
  }

  protected boolean isAnimationEnabledForNode(Node node) {
    return hasPower(node);
  }

  private boolean hasPower(Node node) {
    PowerConsumerComponent powerConsumerComponent = node.<PowerConsumerComponent> getUserData(UserData.POWER_CONSUMER_COMPONENT.name());
    if (powerConsumerComponent != null) {
      PowerState powerState = powerConsumerComponent.getPowerComponent().getPowerState();
      LOG.info("Power of {} is {}", node, powerState);
      return powerState == PowerState.ON;
    }
    LOG.info("Has no power component: {}", node);
    return true;
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.writeSavableArrayList(new ArrayList<>(animationNodes), "animationNodes", null);
  }

  @SuppressWarnings("unchecked")
  @Override
  public void read(JmeImporter im) throws IOException {
    InputCapsule capsule = im.getCapsule(this);
    animationNodes = new HashSet<>(capsule.readSavableArrayList("animationNodes", new ArrayList<>(0)));
  }
}
