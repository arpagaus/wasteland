/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import java.io.IOException;

import app.WastelandApplication;
import app.appstate.InputListenerAppState;

import com.google.common.base.Preconditions;
import com.jme3.audio.AudioNode;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.scene.control.AbstractControl;
import com.jme3.system.AppSettings;
import com.jme3.texture.Image;
import com.jme3.ui.Picture;

public class ShowPictureSelectionListener implements SelectionListener {

  private Picture picture;
  private Node hideNode;
  private boolean hideRootNode;

  private float margin = 0;
  private float widthScale = 1;

  public ShowPictureSelectionListener() {
  }

  public Picture getPicture() {
    return picture;
  }

  public void setPicture(Picture picture) {
    Preconditions.checkNotNull(picture);
    this.picture = picture;
  }

  public Node getHideNode() {
    return hideNode;
  }

  public void setHideNode(Node hideNode) {
    this.hideNode = hideNode;
  }

  public boolean isHideRootNode() {
    return hideRootNode;
  }

  public void setHideRootNode(boolean hideRootNode) {
    this.hideRootNode = hideRootNode;
  }

  public float getMargin() {
    return margin;
  }

  public void setMargin(float margin) {
    this.margin = margin;
  }

  public float getWidthScale() {
    return widthScale;
  }

  public void setWidthScale(float widthScale) {
    this.widthScale = widthScale;
  }

  @Override
  public void onSelection(SelectionEvent event) {
    final WastelandApplication application = WastelandApplication.getApplication();

    alignPictureToCenter(application.getContext().getSettings());

    final InputListenerAppState inputListener = application.getStateManager().getState(InputListenerAppState.class);
    inputListener.setCursorVisibilityModifiable(false);

    final Node guiNode = application.getGuiNode();
    guiNode.attachChild(picture);

    final Node rootNode = application.getRootNode();
    if (hideRootNode) {
      rootNode.setCullHint(CullHint.Always);
    }
    if (hideNode != null) {
      hideNode.setCullHint(CullHint.Always);
      for (Spatial child : hideNode.getChildren()) {
        if (child instanceof AudioNode) {
          ((AudioNode) child).play();
        }
      }
    }

    application.setPostProcessorEnabled(false);

    picture.addControl(new AbstractControl() {

      @Override
      protected void controlUpdate(float tpf) {
        if (inputListener.consumeRelease()) {
          getSpatial().removeControl(this);

          guiNode.detachChild(picture);

          rootNode.setCullHint(CullHint.Inherit);
          if (hideNode != null) {
            hideNode.setCullHint(CullHint.Inherit);
            for (Spatial child : hideNode.getChildren()) {
              if (child instanceof AudioNode) {
                ((AudioNode) child).stop();
              }
            }
          }

          application.setPostProcessorEnabled(true);

          inputListener.setCursorVisibilityModifiable(true);
        }
      }

      @Override
      protected void controlRender(RenderManager rm, ViewPort vp) {
      }
    });
  }

  private void alignPictureToCenter(AppSettings settings) {
    Image image = picture.getMaterial().getTextureParam("Texture").getTextureValue().getImage();

    int height = Math.round(settings.getHeight() * (1f - margin));
    int width = Math.round(1.0f * height / image.getHeight() * image.getWidth() * widthScale);

    if (width > settings.getWidth()) {
      width = settings.getWidth();
      height = Math.round(1.0f * width / image.getWidth() * image.getHeight() * widthScale);
    }

    int positionX = (settings.getWidth() - width) / 2;
    int positionY = (settings.getHeight() - height) / 2;

    picture.setWidth(width);
    picture.setHeight(height);
    picture.setPosition(positionX, positionY);

    picture.getMesh().setBuffer(Type.TexCoord, 2,
        new float[] {
            0, 0,
            1 * widthScale, 0,
            1 * widthScale, 1,
            0, 1 });
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.write(picture, "picture", null);
    capsule.write(hideRootNode, "hideRootNode", false);
  }

  @Override
  public void read(JmeImporter im) throws IOException {
    InputCapsule capsule = im.getCapsule(this);
    picture = (Picture) capsule.readSavable("picture", null);
    hideRootNode = capsule.readBoolean("hideRootNode", false);
  }
}
