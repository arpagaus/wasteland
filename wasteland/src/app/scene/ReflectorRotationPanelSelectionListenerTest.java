/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import static app.scene.AbstractReflectorPanelSelectionListener.*;
import static org.junit.Assert.*;

import org.junit.Test;

import com.jme3.math.FastMath;

public class ReflectorRotationPanelSelectionListenerTest {

  private static final double TEST_ACCURACY = 0.001;

  @Test
  public void testRoundInStepsExactAngles() {
    float angle = roundInSteps(0f, QUARTER_PI);
    assertEquals(0f, angle, TEST_ACCURACY);

    angle = roundInSteps(FastMath.HALF_PI, QUARTER_PI);
    assertEquals(FastMath.HALF_PI, angle, TEST_ACCURACY);

    angle = roundInSteps(FastMath.PI, QUARTER_PI);
    assertEquals(FastMath.PI, angle, TEST_ACCURACY);

    angle = roundInSteps(FastMath.TWO_PI, QUARTER_PI);
    assertEquals(FastMath.TWO_PI, angle, TEST_ACCURACY);

    angle = roundInSteps(QUARTER_PI, QUARTER_PI);
    assertEquals(QUARTER_PI, angle, TEST_ACCURACY);
  }

  @Test
  public void testRoundInStepsToZero() {
    // Positive
    float angle = roundInSteps(0.001f, QUARTER_PI);
    assertEquals(0, angle, TEST_ACCURACY);

    angle = roundInSteps(QUARTER_PI * .5f - 0.001f, QUARTER_PI);
    assertEquals(0, angle, TEST_ACCURACY);

    // Negative
    angle = roundInSteps(-0.001f, QUARTER_PI);
    assertEquals(0, angle, TEST_ACCURACY);

    angle = roundInSteps(-(QUARTER_PI * .5f) + 0.001f, QUARTER_PI);
    assertEquals(0, angle, TEST_ACCURACY);
  }

  @Test
  public void testRoundInStepsToQuarter() {
    float angle = roundInSteps(QUARTER_PI * .5f, QUARTER_PI);
    assertEquals(QUARTER_PI, angle, TEST_ACCURACY);

    angle = roundInSteps(QUARTER_PI - 0.001f, QUARTER_PI);
    assertEquals(QUARTER_PI, angle, TEST_ACCURACY);

    // Due to floating point inaccuracy *0.5f is not enought, thus * 0.51f
    angle = roundInSteps(FastMath.HALF_PI + QUARTER_PI * 0.51f, QUARTER_PI);
    assertEquals(FastMath.HALF_PI + QUARTER_PI, angle, TEST_ACCURACY);
  }

  @Test
  public void testRoundInStepsToQuarterNegative() {
    float angle = roundInSteps(-QUARTER_PI * .5f, QUARTER_PI);
    assertEquals(-QUARTER_PI, angle, TEST_ACCURACY);

    angle = roundInSteps(-QUARTER_PI + 0.001f, QUARTER_PI);
    assertEquals(-QUARTER_PI, angle, TEST_ACCURACY);
  }
}
