/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import resource.Assets;
import app.Launcher;
import app.SpatialState;
import app.WastelandApplication;
import app.appstate.SettingsItem;
import app.control.DesertWindAudioControl;
import app.control.SimpleRefractionControl;
import app.enumeration.CollisionShapeType;
import app.enumeration.UserData;
import app.es.AngleComponent;
import app.es.PowerComponent;
import app.es.PowerComponent.PowerState;
import app.es.PowerConsumerComponent;

import com.jme3.animation.LoopMode;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.scene.control.BillboardControl;
import com.jme3.scene.control.BillboardControl.Alignment;
import com.jme3.system.AppSettings;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;

/*
 * TODO SpatialInterceptor 
 * - Use @SpatialInterceptor on listeners' setters
 */
public class DesertScene extends AbstractScene {

  private DirectionalLight lightSun;
  private DirectionalLight lightTopDow;

  private AssetManager assetManager;

  private AnimSelectionListener[] elevatorListeners = new AnimSelectionListener[4];
  private MotionPathSelectionListener ladderListener;
  private AttachCameraSelectionListener armchairListener;
  private AnimSelectionListener armchairElevatorListener;

  private ReflectorRotationPanelSelectionListener reflectorRotationPanelListener;
  private ReflectorTiltPanelSelectionListener reflectorTiltPanelListener;
  private ReflectorLeverSelectionListener reflectorLeverListener;

  private ShowPictureSelectionListener telescopeRotationSelectionListener;
  private ShowPictureSelectionListener telescopeTiltSelectionListener;

  public DesertScene(AssetManager assetManager, ViewPort viewport) {
    this.assetManager = assetManager;

    this.ladderListener = new MotionPathSelectionListener(3);
    this.armchairListener = new AttachCameraSelectionListener();
    this.armchairElevatorListener = new AnimSelectionListener();

    this.reflectorRotationPanelListener = new ReflectorRotationPanelSelectionListener();
    this.reflectorTiltPanelListener = new ReflectorTiltPanelSelectionListener();
    this.reflectorLeverListener = new ReflectorLeverSelectionListener();

    this.telescopeRotationSelectionListener = new ShowPictureSelectionListener();
    this.telescopeTiltSelectionListener = new ShowPictureSelectionListener();
  }

  private SpatialState getCreateSpatialState(Spatial spatial) {
    SpatialState spatialState = spatial.getUserData(UserData.SPATIAL_STATE.name());
    if (spatialState == null) {
      spatialState = new SpatialState();
      spatial.setUserData(UserData.SPATIAL_STATE.name(), spatialState);
    }
    return spatialState;
  }

  private Material createUnshadedMaterial(String colorMap) {
    AppSettings settings = WastelandApplication.getApplication().getContext().getSettings();

    Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    Texture texture = assetManager.loadTexture(colorMap);
    texture.setAnisotropicFilter(settings.getInteger(SettingsItem.AnisotropicFiltering.name()));
    material.setTexture("ColorMap", texture);
    return material;
  }

  private Material createVertexColorMaterial() {
    Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    material.setBoolean("VertexColor", true);
    return material;
  }

  private int extractIndex(String regex, String nodeName) {
    Matcher matcher = Pattern.compile(regex).matcher(nodeName);
    matcher.matches();
    int index = Integer.parseInt(matcher.group(1));
    return index - 1;
  }

  public AnimSelectionListener getElevatorSelectionListener(int index) {
    if (elevatorListeners[index] == null) {
      elevatorListeners[index] = new AnimSelectionListener();
    }
    return elevatorListeners[index];
  }

  public AnimSelectionListener getElevatorSelectionListener(String regex, String nodeName) {
    return getElevatorSelectionListener(extractIndex(regex, nodeName));
  }

  public DirectionalLight getLightSun() {
    if (lightSun == null) {
      lightSun = new DirectionalLight();
      lightSun.setColor(ColorRGBA.White.mult(.8f));
      lightSun.setDirection(new Vector3f(-.2f, -1, -.2f).normalize());
    }
    return lightSun;
  }

  public DirectionalLight getLightTopDow() {
    if (lightTopDow == null) {
      lightTopDow = new DirectionalLight();
      lightTopDow.setColor(ColorRGBA.White.mult(.5f));
      lightTopDow.setDirection(new Vector3f(0.01f, -1, 0.01f).normalize());
    }
    return lightTopDow;
  }

  private Map<Integer, PowerComponent> elevatorPowerComponents = new HashMap<Integer, PowerComponent>();

  public PowerComponent getElevatorPowerComponent(int index) {
    if (elevatorPowerComponents.containsKey(index)) {
      return elevatorPowerComponents.get(index);
    }

    PowerComponent powerComponent = new PowerComponent();
    elevatorPowerComponents.put(index, powerComponent);

    if (index == 3 || // Initially ON
        Launcher.isDevelopmentMode()) {
      powerComponent.setPowerState(PowerState.ON);
    } else {
      powerComponent.setPowerState(PowerState.OFF);
    }
    return powerComponent;
  }

  @SpatialInterceptor(value = ".+ButtonGeometry1", count = 10)
  public void initAllButtonGeometries(Geometry geometry) {
    geometry.setMaterial(assetManager.loadMaterial(Assets.Materials.wasteland.BlackPlastic_j3m));
    geometry.addLight(getLightTopDow());
  }

  @SpatialInterceptor
  public void initArmchair(Node node) {
    getCreateSpatialState(node).
        setCollisionShapeType(CollisionShapeType.Mesh).
        setAnimationLoopMode(LoopMode.Cycle).setAnimationEnd(false);

    armchairElevatorListener.addAnimationNode(node);

    armchairListener.setParentSpatial(node);
    node.setUserData(UserData.SELECTION_LISTENER.name(), armchairListener);

    node.attachChild(new AudioNode(assetManager, Assets.Sound.Effects.ArmchairElevator_wav));
  }

  @SpatialInterceptor
  public void initArmchairButton(Node node) {
    getCreateSpatialState(node).setAnimationLoopMode(LoopMode.Cycle).setAnimationEnd(false);

    armchairElevatorListener.addAnimationNode(node);
    node.setUserData(UserData.SELECTION_LISTENER.name(), armchairElevatorListener);
  }

  @SpatialInterceptor
  public void initArmchairElevatorCamera(Node node) {
    armchairListener.setAttachCameraNode(node);
  }

  @SpatialInterceptor
  public void initArmchairElevatorCharacter(Node node) {
    armchairListener.setReleaseCharacterNode(node);
  }

  @SpatialInterceptor
  public void initArmchairGeometry1(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.ArmchairColorMap_png));
  }

  @SpatialInterceptor
  public void initBeamUnitBottomGeometry1(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.ReflectorFrameColorMap_png));
  }

  @SpatialInterceptor
  public void initBeamOverground(Node node) {
    BillboardControl control = new BillboardControl();
    control.setAlignment(Alignment.AxialY);
    node.addControl(control);

    node.setCullHint(CullHint.Always);
    node.setMaterial(assetManager.loadMaterial(Assets.Materials.wasteland.Beam_j3m));

    reflectorLeverListener.addBeamNode(node);
  }

  @SpatialInterceptor
  public void initBeamUnderground(Node node) {
    node.setCullHint(CullHint.Always);
    node.setMaterial(assetManager.loadMaterial(Assets.Materials.wasteland.Beam_j3m));

    reflectorLeverListener.addBeamNode(node);
  }

  @SpatialInterceptor
  public void initBridgeGeometry1(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.BridgeColorMap_png));
    geometry.addLight(getLightSun());
  }

  @SpatialInterceptor
  public void initDesert(Node node) {
    getCreateSpatialState(node).setCollisionShapeType(CollisionShapeType.Mesh);
  }

  @SpatialInterceptor
  public void initDesertGeometry1(Geometry geometry) {
    Material material = createUnshadedMaterial(Assets.Textures.wasteland.DesertColorMap_png);
    geometry.setMaterial(material);
  }

  @SpatialInterceptor(count = 6)
  public void initDeskDrawerGeometry1(Geometry geometry) {
    Material material = createUnshadedMaterial(Assets.Textures.wasteland.DeskDrawerColorMap_png);
    geometry.setMaterial(material);
  }

  @SpatialInterceptor
  public void initDeskDrawerMiddleGeometry1(Geometry geometry) {
    Material material = createUnshadedMaterial(Assets.Textures.wasteland.DeskDrawerMiddleColorMap_png);
    geometry.setMaterial(material);
  }

  @SpatialInterceptor(value = "DeskDrawer.+", count = 7)
  public void initDeskDrawers(Node node) {
    getCreateSpatialState(node).setAnimationLoopMode(LoopMode.Cycle).setAnimationEnd(false);

    AnimSelectionListener listener = new AnimSelectionListener();
    listener.addAnimationNode(node);
    node.setUserData(UserData.SELECTION_LISTENER.name(), listener);
  }

  @SpatialInterceptor
  public void initDeskGeometry1(Geometry geometry) {
    Material material = createUnshadedMaterial(Assets.Textures.wasteland.DeskColorMap_png);
    geometry.setMaterial(material);

    getCreateSpatialState(geometry).setCollisionShapeType(CollisionShapeType.Box);
  }

  @SpatialInterceptor(value = "ElevatorButtonBox([1-4])(Bottom|Top|Under)", count = 9)
  public void initElevatorButtonBoxes(Node node) {
    AnimSelectionListener listener = getElevatorSelectionListener("ElevatorButtonBox([1-4])(Bottom|Top|Under)", node.getName());
    node.setUserData(UserData.SELECTION_LISTENER.name(), listener);
  }

  @SpatialInterceptor(value = "ElevatorButtonBoxGeometry1", count = 9)
  public void initElevatorButtonboxGeometry(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.ElevatorButtonBoxColorMap_png));
  }

  @SpatialInterceptor(value = "ElevatorButton([1-4])(Bottom|Top|Under)", count = 9)
  public void initElevatorButtons(Node node) {
    getCreateSpatialState(node).setAnimationLoopMode(LoopMode.Cycle).setAnimationEnd(false);

    AnimSelectionListener listener = getElevatorSelectionListener("ElevatorButton([1-4])(Bottom|Top|Under)", node.getName());
    listener.addAnimationNode(node);
    node.setUserData(UserData.SELECTION_LISTENER.name(), listener);

    AudioNode audioNode = new AudioNode(assetManager, Assets.Sound.Effects.ElevatorButton_wav);
    audioNode.setVolume(0.5f);
    node.attachChild(audioNode);
  }

  @SpatialInterceptor
  public void initElevatorRoom(Node node) {
    node.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.ElevatorRoomColorMap_png));

    getCreateSpatialState(node).setCollisionShapeType(CollisionShapeType.Mesh);
  }

  @SpatialInterceptor(value = "Elevator([1-4])", count = 4)
  public void initElevators(Node node) {
    node.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.ElevatorColorMap_png));

    getCreateSpatialState(node).setCollisionShapeType(CollisionShapeType.Box);
    getCreateSpatialState(node).setAnimationLoopMode(LoopMode.Cycle).setAnimationEnd(false);

    int index = extractIndex("Elevator([1-4])", node.getName());
    AnimSelectionListener listener = getElevatorSelectionListener(index);
    listener.addAnimationNode(node);

    PowerConsumerComponent powerConsumerComponent = new PowerConsumerComponent();
    powerConsumerComponent.setPowerComponent(getElevatorPowerComponent(index));
    node.setUserData(UserData.POWER_CONSUMER_COMPONENT.name(), powerConsumerComponent);

    AudioNode audioNode = new AudioNode(assetManager, Assets.Sound.Effects.Elevator_wav);
    audioNode.setPositional(true);
    audioNode.setRefDistance(0.2f);
    node.attachChild(audioNode);
  }

  @SpatialInterceptor(value = ".+InvisibleWall", count = 1)
  public void initInvisibleWalls(Node node) {
    node.setCullHint(CullHint.Always);
    getCreateSpatialState(node).setCollisionShapeType(CollisionShapeType.Mesh);
  }

  @SpatialInterceptor
  public void initLadder(Node node) {
    node.setUserData(UserData.SELECTION_LISTENER.name(), ladderListener);
    getCreateSpatialState(node).setCollisionShapeType(CollisionShapeType.Mesh);
  }

  @SpatialInterceptor
  public void initLadderGeometry1(Geometry geometry) {
    geometry.setMaterial(assetManager.loadMaterial(Assets.Materials.wasteland.Reflector_j3m));
    geometry.addLight(getLightTopDow());
  }

  @SpatialInterceptor(value = "LadderMotionPath([1-3])", count = 3)
  public void initLadderMotionPath(Node node) {
    ladderListener.setWayPoint(node.getWorldTranslation(), extractIndex("LadderMotionPath([1-3])", node.getName()));
  }

  private void initNote(Node node, String colorMap) {
    initNote(node, colorMap, 0.772727273f);
  }

  private void initNote(Node node, String colorMap, float widthScale) {
    Material material = createUnshadedMaterial(colorMap);
    material.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
    node.setMaterial(material);
    node.setQueueBucket(Bucket.Transparent);

    Texture texture = assetManager.loadTexture(colorMap);
    texture.setWrap(WrapMode.Clamp);
    Picture picture = new Picture("HUD Picture");

    picture.setTexture(assetManager, (Texture2D) texture, true);

    ShowPictureSelectionListener listener = new ShowPictureSelectionListener();
    listener.setPicture(picture);
    listener.setHideNode(node);
    listener.setMargin(0.2f);
    listener.setWidthScale(widthScale);
    node.setUserData(UserData.SELECTION_LISTENER.name(), listener);
  }

  @SpatialInterceptor
  public void initNoteBlank(Node node) {
    initNote(node, Assets.Textures.wasteland.NoteBlank_png);
  }

  @SpatialInterceptor
  public void initNoteElevatorRoom(Node node) {
    initNote(node, Assets.Textures.wasteland.NoteElevatorRoom_png);
  }

  @SpatialInterceptor
  public void initNoteSleepLab(Node node) {
    initNote(node, Assets.Textures.wasteland.NoteSleepLab_png, 1);

    AudioNode audioNode = new AudioNode(assetManager, Assets.Sound.Effects.NoteSleepLab_wav);
    audioNode.setPositional(false);
    audioNode.setReverbEnabled(false);
    node.attachChild(audioNode);
  }

  @SpatialInterceptor
  public void initNoteTempleReflector(Node node) {
    initNote(node, Assets.Textures.wasteland.NoteTempleReflector_png);
  }

  @SpatialInterceptor
  public void initNoteGlavestone01(Node node) {
    initNote(node, Assets.Textures.wasteland.NoteGlavestone01_png);
  }

  @SpatialInterceptor
  public void initNoteGlavestone02(Node node) {
    initNote(node, Assets.Textures.wasteland.NoteGlavestone02_png);
  }

  @SpatialInterceptor(count = 2)
  public void initPictureFrameGeometry1(Geometry geometry) {
    Material material = createUnshadedMaterial(Assets.Textures.wasteland.PictureFrameColorMap_png);
    geometry.setMaterial(material);
  }

  @SpatialInterceptor
  public void initPictureFrameMiddleGeometry1(Geometry geometry) {
    Material material = createUnshadedMaterial(Assets.Textures.wasteland.PictureFrameMiddleColorMap_png);
    geometry.setMaterial(material);
  }

  @SpatialInterceptor
  public void initPictureLiquidGeometry1(Geometry geometry) {
    SimpleRefractionControl control = new SimpleRefractionControl();
    control.setBaseGeometry(geometry);
    geometry.addControl(control);
    reflectorLeverListener.setPictureLiquidControl(control);

    geometry.setCullHint(CullHint.Always);

    PictureLiquidListener pictureLiquidListener = new PictureLiquidListener();
    pictureLiquidListener.setSpatial(geometry);
    geometry.setUserData(UserData.SELECTION_LISTENER.name(), pictureLiquidListener);
  }

  @SpatialInterceptor(count = 4)
  public void initPowerSwitchGeometry1(Geometry geometry) {
    Material material = createUnshadedMaterial(Assets.Textures.wasteland.PowerSwitchColorMap_png);
    geometry.setMaterial(material);
  }

  @SpatialInterceptor(count = 4)
  public void initPowerSwitchHandleGeometry1(Geometry geometry) {
    geometry.setMaterial(assetManager.loadMaterial(Assets.Materials.wasteland.PowerSwitchHandle_j3m));
    geometry.addLight(getLightTopDow());
  }

  @SpatialInterceptor(value = "PowerSwitchHandle([1-4])", count = 4)
  public void initPowerSwitchHandles(Node node) {
    // Entity setup
    int index = extractIndex("PowerSwitchHandle([1-4])", node.getName());
    PowerComponent powerComponent = getElevatorPowerComponent(index);
    node.setUserData(UserData.POWER_COMPONENT.name(), powerComponent);

    // Scene setup
    getCreateSpatialState(node).setAnimationLoopMode(LoopMode.Cycle).setAnimationEnd(powerComponent.getPowerState() == PowerState.ON);

    AnimSelectionListener selectionListener = new AnimSelectionListener();
    selectionListener.addAnimationNode(node);
    node.setUserData(UserData.SELECTION_LISTENER.name(), selectionListener);
  }

  @SpatialInterceptor
  public void initReflector(Node node) {
    getCreateSpatialState(node).setAnimationLoopMode(LoopMode.Cycle).setAnimationEnd(false);
    reflectorLeverListener.setReflectorNode(node);
  }

  @SpatialInterceptor(value = "Reflector(Mount|Joint|Receiver)Geometry1", count = 3)
  public void initReflectorBaseGeometries(Geometry geometry) {
    geometry.setMaterial(assetManager.loadMaterial(Assets.Materials.wasteland.ReflectorBase_j3m));
    geometry.addLight(getLightSun());
  }

  @SpatialInterceptor(value = "Reflector(RotationCrank|TiltLever|Lever)Geometry1", count = 3)
  public void initReflectorCrankTiltGeometries(Geometry geometry) {
    geometry.setMaterial(assetManager.loadMaterial(Assets.Materials.wasteland.ReflectorCrankTilt_j3m));
    geometry.addLight(getLightTopDow());
  }

  @SpatialInterceptor
  public void initReflectorFrameGeometry1(Geometry geometry) {
    Material material = createUnshadedMaterial(Assets.Textures.wasteland.ReflectorFrameColorMap_png);
    geometry.setMaterial(material);
  }

  @SpatialInterceptor
  public void initReflectorGeometry1(Geometry geometry) {
    geometry.setMaterial(assetManager.loadMaterial(Assets.Materials.wasteland.Reflector_j3m));
    geometry.addLight(getLightSun());

    DirectionalLight lightSunInverse = new DirectionalLight();
    lightSunInverse.setDirection(getLightSun().getDirection().negate());
    lightSunInverse.setColor(getLightSun().getColor().clone().multLocal(.5f));
    geometry.addLight(lightSunInverse);
  }

  @SpatialInterceptor
  public void initReflectorJoint(Node node) {
    reflectorLeverListener.setReflectorJointNode(node);
  }

  @SpatialInterceptor
  public void initReflectorLever(Node node) {
    getCreateSpatialState(node).setAnimationLoopMode(LoopMode.Loop).setAnimationEnd(false);

    node.setUserData(UserData.SELECTION_LISTENER.name(), reflectorLeverListener);
    reflectorLeverListener.setReflectorLeverNode(node);
  }

  @SpatialInterceptor
  public void initReflectorLeverPanelGeometry1(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.ReflectorLeverPanelColorMap_png));
  }

  @SpatialInterceptor
  public void initReflectorMount(Node node) {
    getCreateSpatialState(node).setAnimationLoopMode(LoopMode.Cycle).setAnimationEnd(false);
    reflectorLeverListener.setReflectorMountNode(node);
  }

  @SpatialInterceptor(value = "Reflector(TiltPanelPointers|RotationPanelPointers|RotationNeedle)Geometry1", count = 3)
  public void initReflectorPanelPointersGeometries(Geometry geometry) {
    geometry.setMaterial(assetManager.loadMaterial(Assets.Materials.wasteland.ReflectorPanelPointers_j3m));
    geometry.addLight(getLightTopDow());
  }

  @SpatialInterceptor
  public void initReflectorRotationCrank(Node node) {
    reflectorRotationPanelListener.setCrankNode(node);
    getCreateSpatialState(node).setAnimationLoopMode(LoopMode.Loop).setAnimationEnd(false);
  }

  @SpatialInterceptor
  public void initReflectorRotationNeedle(Node node) {
    reflectorRotationPanelListener.setRotationNode(node);

    AngleComponent angleComponent = new AngleComponent();
    node.setUserData(UserData.ANGLE_COMPONENT.name(), angleComponent);

    reflectorLeverListener.setReflectorRotationAngleComponent(angleComponent);
  }

  @SpatialInterceptor
  public void initReflectorRotationPanel(Node node) {
    reflectorRotationPanelListener.setParentSpatial(node);
    node.setUserData(UserData.SELECTION_LISTENER.name(), reflectorRotationPanelListener);
  }

  @SpatialInterceptor
  public void initReflectorRotationPanelCamera(Node node) {
    reflectorRotationPanelListener.setAttachCameraNode(node);
  }

  @SpatialInterceptor
  public void initReflectorRotationPanelGeometry1(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.ReflectorRotTiltPanelColorMap_png));
  }

  @SpatialInterceptor
  public void initReflectorRotationPanelPointers(Node node) {
    node.setUserData(UserData.SELECTION_LISTENER.name(), reflectorRotationPanelListener);
  }

  @SpatialInterceptor
  public void initReflectorTiltLever(Node node) {
    reflectorTiltPanelListener.setRotationNode(node);

    AngleComponent angleComponent = new AngleComponent();
    node.setUserData(UserData.ANGLE_COMPONENT.name(), angleComponent);

    reflectorLeverListener.setReflectorTiltEntityId(angleComponent);
  }

  @SpatialInterceptor
  public void initReflectorTiltPanel(Node node) {
    reflectorTiltPanelListener.setParentSpatial(node);
    node.setUserData(UserData.SELECTION_LISTENER.name(), reflectorTiltPanelListener);
  }

  @SpatialInterceptor
  public void initReflectorTiltPanelCamera(Node node) {
    reflectorTiltPanelListener.setAttachCameraNode(node);
  }

  @SpatialInterceptor
  public void initReflectorTiltPanelGeometry1(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.ReflectorRotTiltPanelColorMap_png));
  }

  @SpatialInterceptor
  public void initReflectorTiltPanelPointers(Node node) {
    node.setUserData(UserData.SELECTION_LISTENER.name(), reflectorTiltPanelListener);
  }

  @SpatialInterceptor
  public void initSpotLightModelGeometry1(Geometry geometry) {
    geometry.setMaterial(createVertexColorMaterial());
  }

  @SpatialInterceptor(count = 2)
  public void initSundailRotationGeometry1(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.SundailRotationColorMap_png));
  }

  @SpatialInterceptor(count = 2)
  public void initSundailTiltGeometry1(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.SundailTiltColorMap_png));
  }

  @SpatialInterceptor
  public void initTableLampGeometry1(Geometry geometry) {
    geometry.setMaterial(createVertexColorMaterial());
  }

  @SpatialInterceptor(count = 2)
  public void initTelescopeGeometry1(Geometry geometry) {
    geometry.setMaterial(assetManager.loadMaterial(Assets.Materials.wasteland.Telescope_j3m));
    geometry.addLight(getLightSun());
  }

  @SpatialInterceptor
  public void initTelescopeSundailRotation(Node node) {
    Picture picture = new Picture("HUD Picture");
    picture.setImage(assetManager, Assets.Pictures.TelescopeSundailRotation_png, true);

    telescopeRotationSelectionListener.setHideRootNode(true);
    telescopeRotationSelectionListener.setPicture(picture);
    node.setUserData(UserData.SELECTION_LISTENER.name(), telescopeRotationSelectionListener);
  }

  @SpatialInterceptor
  public void initTelescopeSundailTilt(Node node) {
    Picture picture = new Picture("HUD Picture");
    picture.setImage(assetManager, Assets.Pictures.TelescopeSundailTilt_png, true);

    telescopeTiltSelectionListener.setHideRootNode(true);
    telescopeTiltSelectionListener.setPicture(picture);
    node.setUserData(UserData.SELECTION_LISTENER.name(), telescopeTiltSelectionListener);
  }

  @SpatialInterceptor
  public void initTemple(Node node) {
    getCreateSpatialState(node).setCollisionShapeType(CollisionShapeType.Mesh);

    AudioNode audioNode = new AudioNode(assetManager, Assets.Sound.Effects.DesertWind_wav);
    audioNode.setLooping(true);
    audioNode.setPositional(false);
    audioNode.setReverbEnabled(false);
    node.attachChild(audioNode);

    DesertWindAudioControl audioControl = new DesertWindAudioControl();
    audioNode.addControl(audioControl);
  }

  @SpatialInterceptor
  public void initTempleGeometry1(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.TempleColorMap_png));
  }

  @SpatialInterceptor
  public void initTempleBasement(Node node) {
    getCreateSpatialState(node).setCollisionShapeType(CollisionShapeType.Mesh);
  }

  @SpatialInterceptor
  public void initTempleBasementGeometry1(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.TempleBasementColorMap_png));
  }

  @SpatialInterceptor
  public void initTempleTower(Node node) {
    getCreateSpatialState(node).setCollisionShapeType(CollisionShapeType.Mesh);
  }

  @SpatialInterceptor
  public void initTempleTowerGeometry1(Geometry geometry) {
    geometry.setMaterial(createUnshadedMaterial(Assets.Textures.wasteland.TempleTowerColorMap_png));
  }
}
