/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import static java.lang.annotation.ElementType.*;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface SpatialInterceptor {
  String value() default "";

  int count() default 1;
}
