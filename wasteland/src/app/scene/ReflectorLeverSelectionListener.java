/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.control.RotationControl;
import app.control.SimpleRefractionControl;
import app.es.AngleComponent;
import app.listener.AbstractAnimEventListener;

import com.google.common.base.Preconditions;
import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.scene.control.AbstractControl;

public class ReflectorLeverSelectionListener implements SelectionListener {

  private static final int REFLECTOR_BASE_ANIMATION_SPEED = 20;

  private static final Logger LOG = LoggerFactory.getLogger(ReflectorLeverSelectionListener.class.getName());

  private final ReflectorLeverAnimDoneListener reflectorLeverAnimDoneListener = new ReflectorLeverAnimDoneListener();
  private final ReflectorMountAnimDoneListener reflectorMountAnimDoneListener = new ReflectorMountAnimDoneListener();
  private final ReflectorAnimDoneListener reflectorAnimDoneListener = new ReflectorAnimDoneListener();

  private boolean enabled = true;

  private Node reflectorLeverNode;
  private Node reflectorMountNode;
  private Node reflectorJointNode;
  private Node reflectorNode;

  private SimpleRefractionControl pictureLiquidControl;
  private Set<Node> beamNodes = new HashSet<Node>();

  private AngleComponent reflectorRotationAngleComponent;
  private AngleComponent reflectorTiltAngleComponent;

  public void setReflectorLeverNode(Node reflectorLeverNode) {
    Preconditions.checkNotNull(reflectorLeverNode);
    this.reflectorLeverNode = reflectorLeverNode;
    this.reflectorLeverNode.getControl(AnimControl.class).addListener(reflectorLeverAnimDoneListener);
  }

  public void setReflectorMountNode(Node reflectorMountNode) {
    Preconditions.checkNotNull(reflectorMountNode);
    this.reflectorMountNode = reflectorMountNode;
    this.reflectorMountNode.getControl(AnimControl.class).addListener(reflectorMountAnimDoneListener);
  }

  public void setReflectorJointNode(Node reflectorJointNode) {
    Preconditions.checkNotNull(reflectorJointNode);
    this.reflectorJointNode = reflectorJointNode;
  }

  public void setReflectorNode(Node reflectorNode) {
    Preconditions.checkNotNull(reflectorNode);
    this.reflectorNode = reflectorNode;
    this.reflectorNode.getControl(AnimControl.class).addListener(reflectorAnimDoneListener);
  }

  public void setPictureLiquidControl(SimpleRefractionControl control) {
    Preconditions.checkNotNull(control);
    this.pictureLiquidControl = control;
  }

  public void addBeamNode(Node node) {
    Preconditions.checkNotNull(node);
    this.beamNodes.add(node);
  }

  public void setReflectorRotationAngleComponent(AngleComponent angleComponent) {
    Preconditions.checkNotNull(angleComponent);
    this.reflectorRotationAngleComponent = angleComponent;
  }

  public void setReflectorTiltEntityId(AngleComponent angleComponent) {
    Preconditions.checkNotNull(angleComponent);
    this.reflectorTiltAngleComponent = angleComponent;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  @Override
  public void onSelection(SelectionEvent event) {
    if (isEnabled()) {
      setEnabled(false);

      reflectorLeverNode.getControl(AnimControl.class).setEnabled(true);
    }
  }

  @Override
  public void write(JmeExporter ex) throws IOException {
    OutputCapsule capsule = ex.getCapsule(this);
    capsule.write(enabled, "enabled", true);
    capsule.write(reflectorLeverNode, "reflectorLeverNode", null);
    capsule.write(reflectorMountNode, "reflectorMountNode", null);
    capsule.write(reflectorJointNode, "reflectorJointNode", null);
    capsule.write(reflectorNode, "reflectorNode", null);
    capsule.write(pictureLiquidControl, "pictureLiquidControl", null);
    capsule.writeSavableArrayList(new ArrayList<Node>(beamNodes), "beamNodes", null);
    capsule.write(reflectorRotationAngleComponent, "reflectorRotationAngleComponent", null);
    capsule.write(reflectorTiltAngleComponent, "reflectorTiltAngleComponent", null);
  }

  @SuppressWarnings("unchecked")
  public void read(com.jme3.export.JmeImporter im) throws IOException {
    InputCapsule capsule = im.getCapsule(this);
    enabled = capsule.readBoolean("enabled", true);
    reflectorLeverNode = (Node) capsule.readSavable("reflectorLeverNode", null);
    reflectorMountNode = (Node) capsule.readSavable("reflectorMountNode", null);
    reflectorJointNode = (Node) capsule.readSavable("reflectorJointNode", null);
    reflectorNode = (Node) capsule.readSavable("reflectorNode", null);
    pictureLiquidControl = (SimpleRefractionControl) capsule.readSavable("pictureLiquidControl", null);
    beamNodes = new HashSet<>(capsule.readSavableArrayList("beamNodes", null));
    reflectorRotationAngleComponent = (AngleComponent) capsule.readSavable("reflectorRotationAngleComponent", null);
    reflectorTiltAngleComponent = (AngleComponent) capsule.readSavable("reflectorTiltAngleComponent", null);

    // Use setters to add the listeners again
    setReflectorLeverNode(reflectorLeverNode);
    setReflectorMountNode(reflectorMountNode);
    setReflectorNode(reflectorNode);
  }

  private final class ReflectorLeverAnimDoneListener extends AbstractAnimEventListener {
    @Override
    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
      super.onAnimCycleDone(control, channel, animName);

      if (reflectorMountNode.getControl(AnimControl.class).getNumChannels() > 0) {
        if (isChannelTimeCloserToZero(reflectorMountNode.getControl(AnimControl.class).getChannel(0))) {
          // If the mount is at its initial position then start its animation
          reflectorMountNode.getControl(AnimControl.class).setEnabled(true);
        }
        else {
          // Otherwise start the rewind
          reflectorNode.addControl(new RewindControl(0));
        }
      }
    }
  }

  private final class ReflectorMountAnimDoneListener extends AbstractAnimEventListener {
    @Override
    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
      super.onAnimCycleDone(control, channel, animName);

      if (isChannelTimeCloserToZero(channel)) {
        setEnabled(true);
      } else {
        reflectorNode.getControl(AnimControl.class).setEnabled(true);
      }
    }
  }

  private final class ReflectorAnimDoneListener extends AbstractAnimEventListener {

    /**
     * This correction is necessary because otherwise the reflector will tilt into the roof
     */
    private static final float TILT_CORRECTION = 0.5f;

    private static final float REWIND_DELAY = 1f;

    private static final float SOLUTION_ROTATION = -247.5f * FastMath.DEG_TO_RAD;
    private static final float SOLUTION_TILT = -45f * FastMath.DEG_TO_RAD;

    private float reflectorBaseRotationDuration;

    @Override
    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
      super.onAnimCycleDone(control, channel, animName);

      if (isChannelTimeCloserToZero(channel)) {
        reflectorMountNode.getControl(AnimControl.class).setEnabled(true);
      }
      else {
        float rotationAngle = reflectorRotationAngleComponent.getAngle();
        float tiltAngle = reflectorTiltAngleComponent.getAngle();

        reflectorBaseRotationDuration = 0;
        rotateReflectorBaseNode(reflectorMountNode, new float[] { 0, rotationAngle, 0 });
        rotateReflectorBaseNode(reflectorJointNode, new float[] { 0, 0, tiltAngle * TILT_CORRECTION });

        LOG.info("roationAngle=" + rotationAngle + ", tiltAngle=" + tiltAngle);
        if (isInputCorrect(rotationAngle, tiltAngle)) {
          LOG.info("Input is correct");

          for (Node beamNode : beamNodes) {
            beamNode.setCullHint(CullHint.Inherit);
          }
          pictureLiquidControl.setShowRefractionQuad(true);
          setEnabled(true);
        }
        else {
          LOG.info("Input is not correct");
          reflectorNode.addControl(new RewindControl(reflectorBaseRotationDuration + REWIND_DELAY));
        }
      }
    }

    private boolean isInputCorrect(float rotationAngle, float tiltAngle) {
      return Math.abs(rotationAngle - SOLUTION_ROTATION) < 0.01f &&
          Math.abs(tiltAngle - SOLUTION_TILT) < 0.01f;
    }

    private void rotateReflectorBaseNode(Node node, float[] rotation) {
      Preconditions.checkArgument(rotation.length == 3, "Rotation mask must have size of 3");

      Quaternion beginRotation = node.getLocalRotation();
      Quaternion endRotation = new Quaternion(rotation);

      LOG.info("Rotating " + node + " from " + beginRotation + " to " + endRotation);

      RotationControl control = new RotationControl(beginRotation, endRotation, REFLECTOR_BASE_ANIMATION_SPEED);
      node.addControl(control);
      reflectorBaseRotationDuration = Math.max(control.getDuration(), reflectorBaseRotationDuration);
    }

  }

  private final class RewindControl extends AbstractControl {

    private final float startThreshold;
    private float time;

    private float reflectorRewindDuration;
    private boolean reflectorRewindTriggered;

    public RewindControl(float startThreshold) {
      this.startThreshold = startThreshold;

      // Turn off the beams
      for (Node beamNode : beamNodes) {
        beamNode.setCullHint(CullHint.Always);
      }
      pictureLiquidControl.setShowRefractionQuad(false);
    }

    @Override
    protected void controlUpdate(float tpf) {
      time = time + tpf;

      if (reflectorRewindTriggered == false && time >= startThreshold) {
        rotateToIdentityRotation(reflectorMountNode);
        rotateToIdentityRotation(reflectorJointNode);

        reflectorRewindTriggered = true;
        LOG.debug("Rotation rewind is triggered. It will take {}", reflectorRewindDuration);
      }
      else if (reflectorRewindTriggered && time >= startThreshold + reflectorRewindDuration) {
        getSpatial().removeControl(this);
        reflectorNode.getControl(AnimControl.class).setEnabled(true);
      }
    }

    private void rotateToIdentityRotation(Node node) {
      RotationControl control = new RotationControl(node.getLocalRotation(), Quaternion.IDENTITY, REFLECTOR_BASE_ANIMATION_SPEED);
      node.addControl(control);

      reflectorRewindDuration = Math.max(control.getDuration(), reflectorRewindDuration);
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }
  }

}
