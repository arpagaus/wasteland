/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.scene;

import java.lang.reflect.Method;
import java.util.List;

import com.jme3.animation.AnimControl;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;

public class AbstractScene {

  public void configure(Node sceneNode) {
    Method[] methods = this.getClass().getMethods();
    for (Method method : methods) {
      if (method.isAnnotationPresent(SpatialInterceptor.class)) {
        SpatialInterceptor annotation = method.getAnnotation(SpatialInterceptor.class);
        String namePattern = annotation.value();

        if (namePattern.equals("")) {
          String methodName = method.getName();
          if (methodName.startsWith("set")) {
            namePattern = methodName.substring(3);
          }
          else if (methodName.startsWith("init")) {
            namePattern = methodName.substring(4);
          }
          else {
            namePattern = methodName;
          }
        }

        @SuppressWarnings("unchecked")
        List<? extends Spatial> matches = sceneNode.descendantMatches((Class<? extends Spatial>) method.getParameterTypes()[0], namePattern);

        if (matches.size() != annotation.count()) {
          throw new IllegalArgumentException("Incorrect count for: '" + namePattern + "', expected: " + annotation.count() + ", actual: " + matches.size());
        }

        for (Spatial spatial : matches) {
          try {
            method.invoke(this, spatial);
          } catch (Exception e) {
            throw new IllegalArgumentException("Failed to invoke " + getClass().getName() + "." + method.getName() + " with " + spatial, e);
          }
        }
      }
    }

    disableAllAnimControls(sceneNode);
  }

  private void disableAllAnimControls(Node sceneNode) {
    sceneNode.depthFirstTraversal(new SceneGraphVisitor() {
      @Override
      public void visit(Spatial spatial) {
        if (spatial.getControl(AnimControl.class) != null) {
          spatial.getControl(AnimControl.class).setEnabled(false);
        }
      }
    });
  }
}
