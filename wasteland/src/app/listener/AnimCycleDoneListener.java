/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.SpatialState;
import app.WastelandApplication;
import app.enumeration.UserData;
import app.es.PowerComponent;
import app.es.PowerComponent.PowerState;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class AnimCycleDoneListener extends AbstractAnimEventListener {

  protected final Logger LOG = LoggerFactory.getLogger(AnimCycleDoneListener.class.getName());

  public AnimCycleDoneListener() {
  }

  @Override
  public void onAnimCycleDone(AnimControl animControl, AnimChannel channel, String animName) {
    super.onAnimCycleDone(animControl, channel, animName);
    animControl.setEnabled(false);

    boolean animationEnd = channel.getTime() > channel.getAnimMaxTime() / 2;
    animControl.getSpatial().<SpatialState> getUserData(UserData.SPATIAL_STATE.name()).setAnimationEnd(animationEnd);

    Spatial spatial = animControl.getSpatial();
    releaseAttachedCharacter(spatial);

    updatePowerComponent(spatial, animationEnd);
  }

  private void updatePowerComponent(Spatial spatial, boolean isOn) {
    PowerComponent powerComponent = spatial.<PowerComponent> getUserData(UserData.POWER_COMPONENT.name());
    if (powerComponent != null) {
      powerComponent.setPowerState(isOn ? PowerState.ON : PowerState.OFF);
    }
  }

  private void releaseAttachedCharacter(Spatial spatial) {
    WastelandApplication application = WastelandApplication.getApplication();
    Node characterNode = application.getCharacterNode();

    if (spatial.equals(characterNode.getParent())) {
      characterNode.setLocalTranslation(characterNode.getParent().getWorldTranslation());
      application.getRootNode().attachChild(characterNode);
      characterNode.getControl(BetterCharacterControl.class).setEnabled(true);
    }
  }
}
