/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.listener;

import com.jme3.cinematic.events.CinematicEvent;
import com.jme3.cinematic.events.CinematicEventListener;

public abstract class AbstractCinematicEventListener implements CinematicEventListener {

  @Override
  public void onPlay(CinematicEvent cinematic) {
  }

  @Override
  public void onPause(CinematicEvent cinematic) {
  }

  @Override
  public void onStop(CinematicEvent cinematic) {
  }
}
