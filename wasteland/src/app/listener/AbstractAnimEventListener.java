/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;

public abstract class AbstractAnimEventListener implements AnimEventListener {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractAnimEventListener.class);

  /**
   * Checks whether the channel is closer to zero or closer to anim max time
   * 
   * @param channel
   * @return
   */
  protected static boolean isChannelTimeCloserToZero(AnimChannel channel) {
    return channel.getTime() < channel.getAnimMaxTime() / 2;
  }

  @Override
  public void onAnimChange(AnimControl animControl, AnimChannel channel, String animName) {
    LOG.debug("{}.onAnimChange on Spatial {}", getClass().getName(), animControl.getSpatial());
  }

  @Override
  public void onAnimCycleDone(AnimControl animControl, AnimChannel channel, String animName) {
    LOG.debug("{}.onAnimCycleDone on {} with time {}", getClass().getName(), animControl.getSpatial(), channel.getTime());
  }
}
