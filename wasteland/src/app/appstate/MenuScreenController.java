/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.appstate;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.prefs.BackingStoreException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import resource.Assets;
import app.Launcher;
import app.WastelandApplication;

import com.google.common.io.ByteStreams;
import com.google.common.io.Files;
import com.jme3.app.state.AppStateManager;
import com.jme3.app.state.ScreenshotAppState;
import com.jme3.asset.AssetManager;
import com.jme3.asset.DesktopAssetManager;
import com.jme3.asset.TextureKey;
import com.jme3.audio.AudioNode;
import com.jme3.system.AppSettings;
import com.jme3.texture.Image;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.events.NiftyMousePrimaryClickedEvent;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class MenuScreenController implements ScreenController {

  private static final Logger LOG = LoggerFactory.getLogger(MenuAppState.class);

  public static final String SCREENSHOT_FILE_NAME = "screenshot";
  public static final String ZIP_ENTRY_SCENE = "scene";

  private enum MainMenuItem {
    Continue, NewGame, Load, Save, Settings, Help, Credits, Quit;

    public static MenuScreenController.MainMenuItem parseId(String id) {
      return valueOf(id.substring("MainMenuItem".length()));
    }

    public String getId() {
      return "MainMenuItem" + name();
    }
  }

  private AssetManager assetManager;
  private AudioNode mainMenuItemClickAudio;

  private AudioNode outroAudio;

  private Nifty nifty;
  private Screen screen;

  private Element panelLoad;
  private Element panelSave;
  private Element panelSettings;
  private Element panelHelp;
  private Element panelCredits;

  private Element popupName;
  private Element popupDismissSave;
  private Element popupSaving;
  private Element popupLoading;

  private boolean isCurrentGameSaved;
  private Runnable dimissSaveRunnable;

  /**
   * All save files sorted with the oldest first and the newest last
   */
  private File[] files;
  private int currentFileLoad;
  private int currentFileSave;

  private AppSettings currentSettings;
  private AppSettings newSettings;

  public MenuScreenController(AppSettings settings, AssetManager assetManager) {
    this.assetManager = assetManager;
    this.currentSettings = settings;
    this.newSettings = (AppSettings) settings.clone();

    this.mainMenuItemClickAudio = new AudioNode(assetManager, Assets.Sound.Effects.multimedia_rollover_049_wav);
    this.mainMenuItemClickAudio.setPositional(false);
    this.mainMenuItemClickAudio.setReverbEnabled(false);

    this.outroAudio = new AudioNode(assetManager, Assets.Sound.outro_ogg);
    this.outroAudio.setPositional(false);
    this.outroAudio.setReverbEnabled(false);
  }

  public String getVersionString() {
    return Launcher.APP_VERSION;
  }

  public String getHeaderHeight() {
    return getHeaderHeightLong() + "px";
  }

  private long getHeaderHeightLong() {
    return Math.round(currentSettings.getHeight() * .25 - 2 * 30);
  }

  public String getHeaderWidth() {
    return Math.round(getHeaderHeightLong() / 165.0 * 619.0) + "px";
  }

  public String getPreviewHeight() {
    return (currentSettings.getHeight() / 3) + "px";
  }

  public String getPreviewWidth() {
    return (currentSettings.getWidth() / 3) + "px";
  }

  @Override
  public void bind(Nifty nifty, Screen screen) {
    this.nifty = nifty;
    this.screen = screen;

    panelLoad = screen.findElementByName("PanelLoad");
    panelSave = screen.findElementByName("PanelSave");
    panelSettings = screen.findElementByName("PanelSettings");
    panelHelp = screen.findElementByName("PanelHelp");
    panelCredits = screen.findElementByName("PanelCredits");

    popupName = nifty.createPopup("PopupName");
    popupDismissSave = nifty.createPopup("PopupDismissSave");
    popupSaving = nifty.createPopup("PopupSaving");
    popupLoading = nifty.createPopup("PopupLoading");

    updateAllSettingsItemTexts();
  }

  private void updateAllSettingsItemTexts() {
    for (SettingsItem settingsItem : SettingsItem.values()) {
      Element element = screen.findElementByName("Settings" + settingsItem);
      if (element == null) {
        LOG.error("No such element " + settingsItem);
      }
      else {
        String text = settingsItem.getText(newSettings);
        element.getRenderer(TextRenderer.class).setText(text);
      }
    }
  }

  private void updateSettingsFile() {
    try {
      newSettings.save(Launcher.SETTINGS_PREF_KEY);
    } catch (BackingStoreException e) {
      LOG.error("Failed to save settings");
    }
  }

  @Override
  public void onStartScreen() {
    isCurrentGameSaved = false;

    reloadSaveFilesList();

    if (files.length > 0) {
      currentFileLoad = files.length - 1;
      updateShownFileLoad(files[currentFileLoad]);

      currentFileSave = files.length - 1;
      updateShownFileSave(files[currentFileSave]);

      screen.findElementByName(MainMenuItem.Load.getId()).enable();
    }
    else {
      updateShowFileSaveNewEntry();

      screen.findElementByName(MainMenuItem.Load.getId()).disable();
    }

    if (!isGameRunning()) {
      screen.findElementByName(MainMenuItem.Continue.getId()).disable();
      screen.findElementByName(MainMenuItem.Save.getId()).disable();
    }
    else {
      screen.findElementByName(MainMenuItem.Continue.getId()).enable();
      screen.findElementByName(MainMenuItem.Save.getId()).enable();
    }
  }

  private boolean isGameRunning() {
    WastelandAppState appState = WastelandApplication.getAppState(WastelandAppState.class);
    return appState != null;
  }

  @Override
  public void onEndScreen() {
  }

  private void reloadSaveFilesList() {
    File saveGameDirectory = new File(getSaveGamePath());
    files = saveGameDirectory.listFiles(new FilenameFilter() {

      @Override
      public boolean accept(File dir, String name) {
        return name.toLowerCase(Locale.US).matches("[\\w-_ ]+.sav");
      }
    });

    if (files == null) {
      LOG.info("No save files found");
      files = new File[0];
    }
    else {
      Arrays.sort(files, new Comparator<File>() {
        @Override
        public int compare(File o1, File o2) {
          return Long.compare(o1.lastModified(), o2.lastModified());
        }
      });
    }
  }

  private void updateShowFileSaveNewEntry() {
    updateShownFile("Save", "Create a new entry", null, null);
  }

  private void updateShownFileSave(File saveFile) {
    updateShownFile("Save", saveFile);
  }

  private void updateShownFileLoad(File saveFile) {
    updateShownFile("Load", saveFile);
  }

  private void updateShownFile(String elementIdPrefix, File saveFile) {
    String name = saveFile.getName().substring(0, saveFile.getName().length() - 4);
    Date date = new Date(saveFile.lastModified());

    BufferedImage bufferedImage = loadScreenshot(saveFile);
    updateShownFile(elementIdPrefix, name, date, bufferedImage);
  }

  private void updateShownFile(String elementIdPrefix, String name, Date date, BufferedImage previewImage) {
    String dateString = date == null ? "" : SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(date);

    Element nameElement = screen.findElementByName(elementIdPrefix + "Name");
    nameElement.getRenderer(TextRenderer.class).setText(name);

    Element dateElement = screen.findElementByName(elementIdPrefix + "Date");
    dateElement.getRenderer(TextRenderer.class).setText(dateString);

    setLoadPreviewImage(elementIdPrefix + "PreviewImage", previewImage);
  }

  private BufferedImage loadScreenshot(File saveFile) {
    try (ZipFile zipFile = new ZipFile(saveFile)) {
      ZipEntry screenshotEntry = zipFile.getEntry(WastelandApplication.SCREENSHOT_FILE_NAME);
      if (screenshotEntry != null) {
        return ImageIO.read(zipFile.getInputStream(screenshotEntry));
      }
    } catch (IOException e) {
      LOG.error("Failed to extract screenshot", e);
    }
    return null;
  }

  private void setLoadPreviewImage(String elementId, BufferedImage bufferedImage) {
    Texture texture = null;
    if (bufferedImage == null) {
      texture = assetManager.loadTexture("Interface/menu/preview-unavailable.png");
    }
    else {
      AWTLoader loader = new AWTLoader();
      Image jmeImage = loader.load(bufferedImage, true);
      texture = new Texture2D(jmeImage);
    }

    Element imageElement = screen.findElementByName(elementId);
    ImageRenderer imageRenderer = imageElement.getRenderer(ImageRenderer.class);

    if (imageRenderer.getImage() != null) {
      NiftyImage currentImage = imageRenderer.getImage();
      imageRenderer.setImage(null);
      currentImage.dispose();
    }

    ((DesktopAssetManager) assetManager).addToCache(new TextureKey(elementId), texture);
    NiftyImage niftyImage = nifty.createImage(elementId, true);

    imageRenderer.setImage(niftyImage);
  }

  @NiftyEventSubscriber(pattern = "MainMenuItem.+")
  public void onClickMainMenuItem(String id, NiftyMousePrimaryClickedEvent event) {
    LOG.debug("onClickMainMenuItem: {}", id);

    MainMenuItem selectedItem = MainMenuItem.parseId(id);

    // Show / hide indicators
    for (MainMenuItem item : MainMenuItem.values()) {
      Element indicatorElement = screen.findElementByName("MainMenuItemIndicator" + item.name());
      if (indicatorElement != null) {
        if (selectedItem == item) {
          indicatorElement.show();
        }
        else {
          indicatorElement.hide();
        }
      }
    }

    // Hide all panels
    panelLoad.hide();
    panelSave.hide();
    panelSettings.hide();
    panelHelp.hide();
    panelCredits.hide();

    switch (selectedItem) {
    case Continue: {
      WastelandApplication.getAppState(MenuAppState.class).toggleVisible();
    }
      break;
    case NewGame: {
      executeDismissSaveRunnable(new Runnable() {
        @Override
        public void run() {
          startNewGame();
        }
      });
    }
      break;
    case Load: {
      panelLoad.show();
    }
      break;
    case Save: {
      panelSave.show();
    }
      break;
    case Settings: {
      panelSettings.show();
    }
      break;
    case Help: {
      panelHelp.show();
    }
      break;
    case Credits: {
      panelCredits.show();
    }
      break;
    case Quit: {
      executeDismissSaveRunnable(new Runnable() {
        @Override
        public void run() {
          WastelandApplication.getApplication().stop();
        }
      });
      return; // Do not play any sound
    }
    default:
      LOG.error("There is no case for {}", id);
    }

    mainMenuItemClickAudio.playInstance();
  }

  private void executeDismissSaveRunnable(Runnable runnable) {
    if (!isCurrentGameSaved && isGameRunning()) {
      nifty.showPopup(screen, popupDismissSave.getId(), null);
      dimissSaveRunnable = runnable;
    }
    else {
      runnable.run();
    }
  }

  private void startNewGame() {
    final Element popupLoadingNewGame = nifty.createPopup("PopupLoadingNewGame");
    popupLoadingNewGame.findElementByName("PopupLoadingNewGameStart").hide();
    nifty.showPopup(nifty.getCurrentScreen(), popupLoadingNewGame.getId(), null);

    WastelandApplication.getApplication().enqueue(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        AppStateManager stateManager = WastelandApplication.getApplication().getStateManager();
        WastelandAppState appState = stateManager.getState(WastelandAppState.class);
        if (appState != null) {
          stateManager.detach(appState);
        }
        stateManager.attach(new WastelandAppState());
        popupLoadingNewGame.findElementByName("PopupLoadingNewGameStart").show();
        return null;
      }
    });
  }

  @NiftyEventSubscriber(id = "LoadArrowRight")
  public void onClickLoadArrowRight(String id, NiftyMousePrimaryClickedEvent event) {
    if (currentFileLoad > 0) {
      currentFileLoad--;
      updateShownFileLoad(files[currentFileLoad]);
      mainMenuItemClickAudio.playInstance();
    }
  }

  @NiftyEventSubscriber(id = "LoadArrowLeft")
  public void onClickLoadArrowLeft(String id, NiftyMousePrimaryClickedEvent event) {
    if (currentFileLoad < files.length - 1) {
      currentFileLoad++;
      updateShownFileLoad(files[currentFileLoad]);
      mainMenuItemClickAudio.playInstance();
    }
  }

  @NiftyEventSubscriber(id = "LoadPreviewImage")
  public void onClickLoadPreviewImage(String id, NiftyMousePrimaryClickedEvent event) {
    if (currentFileLoad >= 0 && currentFileLoad < files.length) {
      nifty.showPopup(nifty.getCurrentScreen(), popupLoading.getId(), null);

      final File file = files[currentFileLoad];
      WastelandApplication.getApplication().enqueue(new Callable<Void>() {
        @Override
        public Void call() throws Exception {
          loadGame(file);
          nifty.closePopup(popupLoading.getId());
          return null;
        }
      });
    }
  }

  /**
   * Saves an existing file or shows a popup
   * 
   * @param id
   * @param event
   */
  @NiftyEventSubscriber(id = "SavePreviewImage")
  public void onClickSavePreviewImage(String id, NiftyMousePrimaryClickedEvent event) {
    if (currentFileSave >= 0 && currentFileSave < files.length) {
      File file = files[currentFileSave];
      saveGame(
          Files.getNameWithoutExtension(file.getName()),
          new Callable<Void>() {
            public Void call() throws Exception {
              reloadSaveFilesList();
              currentFileSave = files.length - 1; // The saved file will now be the newest
              updateShownFileSave(files[currentFileSave]);
              return null;
            }
          });
    }
    else if (currentFileSave == files.length) {
      nifty.showPopup(nifty.getCurrentScreen(), popupName.getId(), null);
    }
  }

  @NiftyEventSubscriber(id = "SaveArrowRight")
  public void onClickSaveArrowRight(String id, NiftyMousePrimaryClickedEvent event) {
    if (currentFileSave > 0) {
      currentFileSave--;
      updateShownFileSave(files[currentFileSave]);
      mainMenuItemClickAudio.playInstance();
    }
  }

  @NiftyEventSubscriber(id = "SaveArrowLeft")
  public void onClickSaveArrowLeft(String id, NiftyMousePrimaryClickedEvent event) {
    if (currentFileSave < files.length - 1) {
      currentFileSave++;
      updateShownFileSave(files[currentFileSave]);
      mainMenuItemClickAudio.playInstance();
    }
    else if (currentFileSave == files.length - 1) {
      currentFileSave++;
      updateShowFileSaveNewEntry();
      mainMenuItemClickAudio.playInstance();
    }
  }

  @NiftyEventSubscriber(pattern = "SettingsIncrease.+")
  public void onSettingsIncrease(String id, NiftyMousePrimaryClickedEvent event) {
    SettingsItem settingsItem = SettingsItem.valueOf(id.substring("SettingsIncrease".length()));
    settingsItem.increaseValue(newSettings);
    updateAllSettingsItemTexts();
    updateSettingsFile();

    mainMenuItemClickAudio.playInstance();
  }

  @NiftyEventSubscriber(pattern = "SettingsDecrease.+")
  public void onSettingsDecrease(String id, NiftyMousePrimaryClickedEvent event) {
    SettingsItem settingsItem = SettingsItem.valueOf(id.substring("SettingsDecrease".length()));
    settingsItem.decreaseValue(newSettings);
    updateAllSettingsItemTexts();
    updateSettingsFile();

    mainMenuItemClickAudio.playInstance();
  }

  @NiftyEventSubscriber(pattern = "PopupLoadingNewGameStart")
  public void onPopupLoadingNewGameStart(String id, NiftyMousePrimaryClickedEvent event) {
    Element popup = nifty.getTopMostPopup();
    if (popup == null) {
      LOG.error("There is no popup");
      return;
    }
    nifty.closePopup(popup.getId());

    WastelandApplication.getAppState(MenuAppState.class).toggleVisible();
  }

  @NiftyEventSubscriber(pattern = "PopupDismissSave.+")
  public void onPopupDismissSave(String id, NiftyMousePrimaryClickedEvent event) {
    Element popup = nifty.getTopMostPopup();
    if (popup == null) {
      LOG.error("There is no popup");
      return;
    }
    nifty.closePopup(popup.getId());

    if (id.endsWith("Ok")) {
      if (dimissSaveRunnable != null) {
        dimissSaveRunnable.run();
      }
      else {
        LOG.error("There is no dimissSaveCallable set");
      }
    }
    dimissSaveRunnable = null;
  }

  @NiftyEventSubscriber(pattern = "PopupName.+")
  public void onPopupName(String id, NiftyMousePrimaryClickedEvent event) {
    Element popup = nifty.getTopMostPopup();
    if (popup == null) {
      LOG.error("There is no popup");
      return;
    }
    nifty.closePopup(popup.getId());

    if (id.endsWith("Ok")) {
      TextField textField = popup.findNiftyControl("PopupNameValue", TextField.class);
      if (textField == null) {
        LOG.error("There is no PopupNameValue control");
        return;
      }

      String name = textField.getRealText();
      textField.setText(""); // Clear the text
      saveGame(
          name,
          new Callable<Void>() {
            public Void call() throws Exception {
              reloadSaveFilesList();
              currentFileSave = files.length - 1;
              updateShownFileSave(files[currentFileSave]);

              if (files.length == 1) { // First save
                currentFileLoad = currentFileSave;
                updateShownFileLoad(files[currentFileLoad]);
              }
              screen.findElementByName(MainMenuItem.Load.getId()).enable();

              return null;
            }
          });
    }
  }

  public String getSaveGamePath() {
    return System.getProperty("user.home") + File.separator + ".wasteland" + File.separator + "save" + File.separator;
  }

  public String getScreenshotPath() {
    return getSaveGamePath() + File.separator + SCREENSHOT_FILE_NAME + ".png";
  }

  public void saveGame(final String name, final Callable<?> callback) {
    nifty.showPopup(nifty.getCurrentScreen(), popupSaving.getId(), null);

    ScreenshotAppState screenshotAppState = WastelandApplication.getAppState(ScreenshotAppState.class);
    if (screenshotAppState != null) {
      WastelandApplication.getApplication().setPostProcessorEnabled(false);

      new File(getSaveGamePath()).mkdirs();
      screenshotAppState.setFilePath(getSaveGamePath());
      screenshotAppState.setIsNumbered(false);
      screenshotAppState.setFileName(SCREENSHOT_FILE_NAME);
      screenshotAppState.takeScreenshot();
    }

    // Enqueue to ensure the screenshot was updated
    WastelandApplication.getApplication().enqueue(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        WastelandApplication.getApplication().setPostProcessorEnabled(true);
        saveGameInternal(name);
        if (callback != null) {
          callback.call();
        }

        nifty.closePopup(popupSaving.getId());

        return null;
      }
    });
  }

  private void saveGameInternal(String name) {
    isCurrentGameSaved = true;

    long time = System.currentTimeMillis();

    name = name.replaceAll("[^\\w-_ ]", "_"); // Replace special characters

    WastelandAppState appState = WastelandApplication.getAppState(WastelandAppState.class);
    if (appState == null) {
      LOG.error("No appState");
      return;
    }

    try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(getSaveGamePath() + File.separator + name + ".sav"));
        FileInputStream screenshotInputStream = new FileInputStream(getScreenshotPath());) {
      // Screenshot
      ZipEntry screenshotEntry = new ZipEntry(SCREENSHOT_FILE_NAME);
      zipOutputStream.putNextEntry(screenshotEntry);

      ByteStreams.copy(screenshotInputStream, zipOutputStream);
      screenshotInputStream.close();

      // Scene Node
      ZipEntry sceneEntry = new ZipEntry(ZIP_ENTRY_SCENE);
      zipOutputStream.putNextEntry(sceneEntry);
      appState.saveGame(zipOutputStream);

      LOG.info("Saved game {} in {}ms", name, System.currentTimeMillis() - time);
    } catch (IOException e) {
      LOG.error("Failed to save game", e);
    }
  }

  public void loadGame(File file) {
    long time = System.currentTimeMillis();

    AppStateManager stateManager = WastelandApplication.getApplication().getStateManager();
    WastelandAppState appState = stateManager.getState(WastelandAppState.class);

    // Remove an existing appState
    if (appState != null) {
      stateManager.detach(appState);
    }

    appState = new WastelandAppState();

    try (ZipFile zipFile = new ZipFile(file);) {
      ZipEntry sceneEntry = zipFile.getEntry(ZIP_ENTRY_SCENE);

      appState.loadGame(zipFile.getInputStream(sceneEntry), assetManager);

      LOG.info("Loaded game {} in {}ms", file.getName(), System.currentTimeMillis() - time);
    } catch (IOException e) {
      LOG.error("Failed to load game");
    }

    stateManager.attach(appState);

    if (stateManager.getState(MenuAppState.class) != null) {
      stateManager.getState(MenuAppState.class).toggleVisible();
    }
  }

  public void showOutro() {
    Element popup = nifty.createPopup("PopupOutro");
    final List<Element> elements = popup.getElements().get(0).getElements();
    for (Element element : elements) {
      element.hide();
    }

    final AtomicInteger index = new AtomicInteger();
    final Timer timer = new Timer(true);
    timer.scheduleAtFixedRate(new TimerTask() {
      @Override
      public void run() {
        WastelandApplication.getApplication().enqueue(new Callable<Void>() {
          @Override
          public Void call() throws Exception {
            if (index.get() == elements.size()) {
              nifty.closePopup(nifty.getTopMostPopup().getId());
              outroAudio.stop();
              timer.cancel();
            }
            else {
              if (index.get() > 0) {
                elements.get(index.get() - 1).hide();
              }
              elements.get(index.get()).show();
              index.incrementAndGet();
            }
            return null;
          }
        });
      }
    }, new Date(), (long) (outroAudio.getAudioData().getDuration() / elements.size() * 1000));

    nifty.showPopup(screen, popup.getId(), null);
    outroAudio.play();
  }
}
