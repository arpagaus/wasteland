/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.appstate;

import java.util.concurrent.atomic.AtomicReference;

import resource.Assets;
import app.Analytics;
import app.enumeration.UserData;
import app.scene.SelectionListener;
import app.scene.SelectionListener.SelectionEvent;

import com.brsanthu.googleanalytics.EventHit;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.TextureKey;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.math.Transform;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitorAdapter;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.system.AppSettings;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;

public class CharacterSelectionAppState extends AbstractAppState {
  private static final float MAX_PLAYER_RANGE = 1.8f;

  private SimpleApplication application;
  private AppStateManager stateManager;

  private Node characterNode;
  private Camera camera;

  private Node cursorNode;
  private Picture cursorNormalPicture;
  private Picture cursorSelectablePicture;

  @Override
  public void initialize(AppStateManager stateManager, Application app) {
    super.initialize(stateManager, app);
    this.application = (SimpleApplication) app;
    this.stateManager = stateManager;

    characterNode = findCharacterNode(application.getRootNode());
    if (characterNode == null) {
      throw new IllegalStateException("The given appliation does not contain any Node with a BetterCharacterControl");
    }

    camera = application.getCamera();

    AppSettings settings = application.getContext().getSettings();

    // Cursors
    TextureKey key = new TextureKey(Assets.Interface.Cursor.cursor_normal_png, true);
    Texture2D texture = (Texture2D) application.getAssetManager().loadTexture(key);
    int dimension = texture.getImage().getHeight();

    cursorNode = new Node("Cursor");
    cursorNode.setLocalTranslation(settings.getWidth() / 2 - (dimension / 2), settings.getHeight() / 2 - (dimension / 2), 0);
    application.getGuiNode().attachChild(cursorNode);

    cursorNormalPicture = new Picture("CursorNormal");
    cursorNormalPicture.setTexture(application.getAssetManager(), texture, true);
    cursorNormalPicture.setWidth(dimension);
    cursorNormalPicture.setHeight(dimension);
    cursorNode.attachChild(cursorNormalPicture);

    cursorSelectablePicture = new Picture("CursorSelectable");
    cursorSelectablePicture.setImage(application.getAssetManager(), Assets.Interface.Cursor.cursor_selectable_png, true);
    cursorSelectablePicture.setWidth(dimension);
    cursorSelectablePicture.setHeight(dimension);
    cursorSelectablePicture.setCullHint(CullHint.Always);
    cursorNode.attachChild(cursorSelectablePicture);
  }

  private Node findCharacterNode(Node rootNode) {
    final AtomicReference<Node> holder = new AtomicReference<Node>();
    rootNode.breadthFirstTraversal(new SceneGraphVisitorAdapter() {
      @Override
      public void visit(Node node) {
        if (node.getControl(BetterCharacterControl.class) != null) {
          holder.set(node);
        }
      }
    });
    return holder.get();
  }

  @Override
  public void cleanup() {
    super.cleanup();

    application.getGuiNode().detachChild(cursorNode);
    cursorNode = null;
    cursorNormalPicture = null;
    cursorSelectablePicture = null;

    camera = null;

    stateManager = null;
    application = null;
  }

  private Vector2f click2d = new Vector2f();

  @Override
  public void update(float tpf) {
    if (!isEnabled()) {
      return;
    }

    InputListenerAppState inputListener = stateManager.getState(InputListenerAppState.class);
    if (inputListener != null && (inputListener.isMoving() || inputListener.isCursorVisible())) {
      cursorNode.setCullHint(CullHint.Always);
      if (inputListener.isMoving()) {
        // Selection is not possible
        return;
      }
    } else {
      cursorNode.setCullHint(CullHint.Inherit);
    }

    Vector3f origin;
    Vector3f direction;
    if (inputListener.isCursorVisible()) {
      click2d.set(inputListener.getSelectCoordinatesX(), inputListener.getSelectCoordinatesY());
      origin = camera.getWorldCoordinates(click2d, 0f);
      direction = camera.getWorldCoordinates(click2d, 1f).subtractLocal(origin).normalizeLocal();
    }
    else {
      origin = camera.getLocation();
      direction = camera.getDirection();
    }

    final Ray ray = new Ray(origin, direction);
    final CollisionResults results = new CollisionResults();
    application.getRootNode().breadthFirstTraversal(new SceneGraphVisitorAdapter() {
      @Override
      public void visit(Geometry geom) {
        if (geom.getParent().getUserDataKeys().contains(UserData.SELECTION_LISTENER.name()) ||
            geom.getUserDataKeys().contains(UserData.SELECTION_LISTENER.name())) {
          int count;
          if ((count = geom.collideWith(ray, results)) > 0) {
            results.getCollisionDirect(results.size() - 1).setGeometry(geom);
            if (count > 1) {
              results.getCollisionDirect(results.size() - 2).setGeometry(geom);
            }
          }
        } else {
          geom.collideWith(ray, results);
        }
      }
    });

    boolean select = inputListener.consumeSelect();

    CollisionResult closestCollision = results.getClosestCollision();
    if (results.size() == 0 || closestCollision.getDistance() > MAX_PLAYER_RANGE) {
      showSelectableCursor(false);
      postAsyncEmptySelection(select);
    } else {
      Geometry geometry = closestCollision.getGeometry();
      SelectionListener selectionListener = geometry.getParent().<SelectionListener> getUserData(UserData.SELECTION_LISTENER.name());

      // Try geometry if the listeners is not on the node
      if (selectionListener == null) {
        selectionListener = geometry.<SelectionListener> getUserData(UserData.SELECTION_LISTENER.name());
      }

      if (selectionListener == null) {
        showSelectableCursor(false);
        postAsyncEmptySelection(select);
      }
      else {
        showSelectableCursor(true);
        if (select) {
          selectionListener.onSelection(new SelectionEvent(closestCollision.getContactPoint()));
          Analytics.postAsync(new EventHit("Action", "Selection", geometry.getName(), (int) (closestCollision.getDistance() * 100)));
        }
      }
    }
  }

  private void postAsyncEmptySelection(boolean select) {
    if (select) {
      Analytics.postAsync(new EventHit("Action", "EmptySelection", new Transform(camera.getLocation(), camera.getRotation()).toString(), 1));
    }
  }

  private void showSelectableCursor(boolean show) {
    if (show) {
      cursorNormalPicture.setCullHint(CullHint.Always);
      cursorSelectablePicture.setCullHint(CullHint.Never);
    }
    else {
      cursorNormalPicture.setCullHint(CullHint.Never);
      cursorSelectablePicture.setCullHint(CullHint.Always);
    }
  }

}
