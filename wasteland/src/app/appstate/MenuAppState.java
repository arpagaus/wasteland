/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.appstate;

import resource.Assets;
import app.Launcher;
import app.WastelandApplication;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.system.AppSettings;

import de.lessvoid.nifty.Nifty;

public class MenuAppState extends AbstractAppState {

  private static final String SCREEN_MENU = "menu";
  private static final String SCREEN_EMPTY = "empty";

  private static final boolean DEBUGGING_COLORS = false;

  private Application application;

  /**
   * This custom viewport is necessary to make sure Nifty GUI is rendered on top of the {@link Application#getGuiViewPort()}
   */
  private ViewPort niftyViewPort;
  private NiftyJmeDisplay niftyJmeDisplay;
  private MenuScreenController menuScreenController;

  @Override
  public void initialize(AppStateManager stateManager, Application application) {
    super.initialize(stateManager, application);
    this.application = application;

    AppSettings settings = application.getContext().getSettings();
    Camera niftyGuiCam = new Camera(settings.getWidth(), settings.getHeight());
    niftyViewPort = application.getRenderManager().createPostView("Nifty GUI Default", niftyGuiCam);
    niftyViewPort.setClearFlags(false, false, false);

    menuScreenController = new MenuScreenController(settings, application.getAssetManager());

    niftyJmeDisplay = new NiftyJmeDisplay(
        application.getAssetManager(),
        application.getInputManager(),
        application.getAudioRenderer(),
        niftyViewPort);

    Nifty nifty = niftyJmeDisplay.getNifty();

    // Validate
    if (Launcher.isDevelopmentMode()) {
      try {
        nifty.validateXml(Assets.Interface.menu_xml);
      } catch (Exception e) {
        throw new IllegalArgumentException(e);
      }
    }

    nifty.setDebugOptionPanelColors(Launcher.isDevelopmentMode() && DEBUGGING_COLORS);
    nifty.fromXml(Assets.Interface.menu_xml, SCREEN_EMPTY, menuScreenController);
    niftyViewPort.addProcessor(niftyJmeDisplay);

    toggleVisible(); // Set initially visible
  }

  @Override
  public void cleanup() {
    super.cleanup();

    niftyViewPort.removeProcessor(niftyJmeDisplay);
    niftyJmeDisplay.getNifty().exit();
    application.getRenderManager().removePostView(niftyViewPort);

    application = null;
    menuScreenController = null;
    niftyJmeDisplay = null;
    niftyViewPort = null;
  }

  private boolean cursorVisibilityModifiable;
  private boolean cursorVisible;

  public void toggleVisible() {
    Nifty nifty = niftyJmeDisplay.getNifty();
    boolean isMenuCurrentlyVisible = nifty.getCurrentScreen().getScreenId().equals(SCREEN_MENU);

    if (isMenuCurrentlyVisible && WastelandApplication.getAppState(WastelandAppState.class) == null) {
      // Do not hide the menu screen if there is not scene app state
      return;
    }

    if (isMenuCurrentlyVisible) {
      nifty.gotoScreen(SCREEN_EMPTY);
    }
    else {
      nifty.gotoScreen(SCREEN_MENU);
    }

    InputListenerAppState inputListener = application.getStateManager().getState(InputListenerAppState.class);
    if (inputListener != null) {
      if (isMenuCurrentlyVisible) {
        // Restore values
        inputListener.setCursorVisible(cursorVisible);
        inputListener.setCursorVisibilityModifiable(cursorVisibilityModifiable);

      }
      else {
        // Store the current values
        cursorVisibilityModifiable = inputListener.isCursorVisibilityModifiable();
        cursorVisible = inputListener.isCursorVisible();

        inputListener.setCursorVisible(true);
        inputListener.setCursorVisibilityModifiable(false);
      }
    }
  }

  public void showOutro() {
    menuScreenController.showOutro();
  }
}
