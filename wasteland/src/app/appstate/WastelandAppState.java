package app.appstate;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import resource.Assets;
import app.ModelCreator;
import app.SpatialState;
import app.WastelandApplication;
import app.enumeration.UserData;
import app.listener.AnimCycleDoneListener;
import app.processor.SimpleRefractionProcessor;
import app.scene.DesertScene;

import com.google.common.base.Preconditions;
import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.bullet.control.PhysicsControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.export.binary.BinaryExporter;
import com.jme3.export.binary.BinaryImporter;
import com.jme3.light.AmbientLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.post.filters.FadeFilter;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.SceneGraphVisitorAdapter;
import com.jme3.scene.Spatial;
import com.jme3.util.SkyFactory;

public class WastelandAppState extends AbstractAppState {

  private static final String PLAYER_TRANSFORM_STORE = "Player";

  private static final Logger LOG = LoggerFactory.getLogger(WastelandAppState.class);

  private SimpleRefractionProcessor refractionProcessor;
  private Node baseNode;
  private Node sceneNode;
  private Spatial skySpatial;

  public SimpleRefractionProcessor getRefractionProcessor() {
    return refractionProcessor;
  }

  public Node getBaseNode() {
    return baseNode;
  }

  @Override
  public void initialize(AppStateManager stateManager, Application app) {
    LOG.info("initializing");
    super.initialize(stateManager, app);

    refractionProcessor = new SimpleRefractionProcessor(app.getAssetManager());
    app.getViewPort().addProcessor(refractionProcessor);

    baseNode = new Node(getClass().getSimpleName());
    sceneNode = createSceneNode(app);

    refractionProcessor.setRefractionScene(sceneNode);

    // Preload the scene to prevent sudden lags due to lazy loading textures
    app.getRenderManager().preloadScene(sceneNode);

    Spatial playerTransformSpatial = sceneNode.getChild(PLAYER_TRANSFORM_STORE);
    if (playerTransformSpatial == null) {
      LOG.error("There is no player node");
    }
    else {
      WastelandApplication.getApplication().getCameraNode().setLocalRotation(playerTransformSpatial.getLocalRotation());

      Node characterNode = WastelandApplication.getApplication().getCharacterNode();
      BetterCharacterControl characterControl = characterNode.getControl(BetterCharacterControl.class);
      if (characterControl != null) {
        characterNode.removeControl(characterControl);
        characterNode.setLocalTranslation(playerTransformSpatial.getLocalTranslation());
        characterNode.addControl(characterControl);
      }
    }

    skySpatial = createSky(app.getAssetManager());

    baseNode.attachChild(sceneNode);
    baseNode.attachChild(skySpatial);

    WastelandApplication.getApplication().getRootNode().attachChild(baseNode);

    final FadeFilter filter = WastelandApplication.getApplication().getPostProcessor().getFilter(FadeFilter.class);
    if (filter != null) {
      filter.setValue(1);
    }
  }

  @Override
  public void cleanup() {
    LOG.info("cleaning up");
    super.cleanup();

    BulletAppState bulletAppState = WastelandApplication.getAppState(BulletAppState.class);
    if (bulletAppState != null && bulletAppState.isInitialized()) {
      bulletAppState.getPhysicsSpace().removeAll(sceneNode);
    }

    sceneNode.depthFirstTraversal(new SceneGraphVisitorAdapter() {
      @Override
      public void visit(Node node) {
        if (node instanceof AudioNode) {
          ((AudioNode) node).stop();
        }
      }
    });

    WastelandApplication.getApplication().getRootNode().detachChild(baseNode);
    WastelandApplication.getApplication().getViewPort().removeProcessor(refractionProcessor);

    this.refractionProcessor = null;
    this.baseNode = null;
    this.sceneNode = null;
    this.skySpatial = null;
  }

  @Override
  public void stateAttached(AppStateManager stateManager) {
    super.stateAttached(stateManager);

    BulletAppState bulletAppState = stateManager.getState(BulletAppState.class);
    if (bulletAppState != null) {
      bulletAppState.setEnabled(true);
    }
  }

  @Override
  public void stateDetached(AppStateManager stateManager) {
    super.stateDetached(stateManager);

    BulletAppState bulletAppState = stateManager.getState(BulletAppState.class);
    if (bulletAppState != null) {
      bulletAppState.setEnabled(false);
    }
  }

  private Node createSceneNode(final Application app) {
    // If there is no scene Node yet the load the new one
    if (sceneNode == null) {
      sceneNode = ModelCreator.loadBlenderModel(Assets.Models.wasteland_jme_blend, app.getAssetManager());
      new DesertScene(app.getAssetManager(), app.getViewPort()).configure(sceneNode);

      AmbientLight ambientLight = new AmbientLight();
      ambientLight.setColor(ColorRGBA.White.mult(1.3f));
      sceneNode.addLight(ambientLight);
    }

    sceneNode.depthFirstTraversal(new SceneGraphVisitor() {
      private PhysicsSpace physicsSpace = app.getStateManager().getState(BulletAppState.class).getPhysicsSpace();

      @Override
      public void visit(Spatial spatial) {
        SpatialState spatialState = spatial.getUserData(UserData.SPATIAL_STATE.name());
        if (spatialState != null) {

          if (spatialState.getCollisionShapeType() != null) {
            CollisionShape collisionShape = spatialState.getCollisionShapeType().createCollisionShape(spatial);
            RigidBodyControl control = new RigidBodyControl(collisionShape, 0);
            control.setKinematic(true);
            control.setKinematicSpatial(true);
            spatial.addControl(control);
            physicsSpace.add(control);
          }

          if (spatialState.getAnimationLoopMode() != null) {
            AnimControl control = spatial.getControl(AnimControl.class);
            Preconditions.checkNotNull(control, spatial + " has no AnimControl");

            AnimChannel channel = control.createChannel();
            String animationName = control.getAnimationNames().iterator().next();
            channel.setAnim(animationName, 0);
            channel.setLoopMode(spatialState.getAnimationLoopMode());

            if (spatialState.isAnimationEnd()) {
              LOG.debug("Setting animation {} to the end", animationName);

              channel.setTime(channel.getAnimMaxTime());

              boolean enabled = control.isEnabled();
              control.setEnabled(true);
              control.update(0);
              control.setEnabled(enabled);
            }
            control.addListener(new AnimCycleDoneListener());
          }
        }
      }
    });

    return sceneNode;
  }

  private Spatial createSky(AssetManager assetManager) {
    Spatial sky = SkyFactory.createSky(
        assetManager,
        assetManager.loadTexture(Assets.Textures.sky.skymap_dds),
        new Vector3f(-1, 1, 1), // Due to strange setup
        false);
    return sky;
  }

  public void saveGame(OutputStream outputStream) throws IOException {
    Spatial saveSceneNode = sceneNode;
    final Map<Spatial, PhysicsControl> physicsControls = new HashMap<Spatial, PhysicsControl>();
    saveSceneNode.depthFirstTraversal(new SceneGraphVisitor() {

      @Override
      public void visit(Spatial spatial) {
        PhysicsControl control = spatial.getControl(PhysicsControl.class);
        if (control != null) {
          spatial.removeControl(control);
          physicsControls.put(spatial, control);
        }
      }
    });

    Spatial playerTransformSpatial = sceneNode.getChild(PLAYER_TRANSFORM_STORE);
    if (playerTransformSpatial != null) {
      playerTransformSpatial.setLocalTranslation(WastelandApplication.getApplication().getCharacterNode().getWorldTranslation());
      playerTransformSpatial.setLocalRotation(WastelandApplication.getApplication().getCameraNode().getLocalRotation());
      LOG.debug("Saved player transform: {}, {}", playerTransformSpatial.getLocalTransform(), playerTransformSpatial.getWorldTransform());
    }
    else {
      LOG.error("There is no player node");
    }

    BinaryExporter.getInstance().save(saveSceneNode, outputStream);

    // Reattach physics controls
    for (Map.Entry<Spatial, PhysicsControl> entry : physicsControls.entrySet()) {
      entry.getKey().addControl(entry.getValue());
    }
  }

  public void loadGame(InputStream inputStream, AssetManager assetManager) throws IOException {
    if (isInitialized()) {
      throw new IllegalStateException("Cannot load game when already initialized");
    }

    BinaryImporter importer = BinaryImporter.getInstance();
    importer.setAssetManager(assetManager);
    Node sceneNode = (Node) importer.load(inputStream);

    this.sceneNode = sceneNode;
  }
}
