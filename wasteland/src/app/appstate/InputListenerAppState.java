/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.appstate;

import resource.Assets;
import app.Launcher;
import app.WastelandApplication;

import com.jme3.app.Application;
import com.jme3.app.StatsAppState;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.cursors.plugins.JmeCursor;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.TouchInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.controls.TouchListener;
import com.jme3.input.controls.TouchTrigger;
import com.jme3.input.controls.Trigger;
import com.jme3.input.event.TouchEvent;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.util.TempVars;

public class InputListenerAppState extends AbstractAppState implements ActionListener, TouchListener, AnalogListener {

  private static final int NO_TOUCH_DOWN = -1;
  private static final int TOUCH_DOWN_THREASHOLD = 800;

  public enum InputMappingName {
    MOVE_FORWARD {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] { new KeyTrigger(KeyInput.KEY_W) };
      }
    },
    MOVE_BACKWARD {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] { new KeyTrigger(KeyInput.KEY_S) };
      }
    },
    MOVE_LEFT {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] { new KeyTrigger(KeyInput.KEY_A) };
      }
    },
    MOVE_RIGHT {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] { new KeyTrigger(KeyInput.KEY_D) };
      }
    },
    ROTATE_UP {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] {
            new MouseAxisTrigger(MouseInput.AXIS_Y, false),
            new KeyTrigger(KeyInput.KEY_UP) };
      }
    },
    ROTATE_DOWN {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] {
            new MouseAxisTrigger(MouseInput.AXIS_Y, true),
            new KeyTrigger(KeyInput.KEY_DOWN) };
      }
    },
    ROTATE_LEFT {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] {
            new MouseAxisTrigger(MouseInput.AXIS_X, true),
            new KeyTrigger(KeyInput.KEY_LEFT) };
      }
    },
    ROTATE_RIGHT {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] {
            new MouseAxisTrigger(MouseInput.AXIS_X, false),
            new KeyTrigger(KeyInput.KEY_RIGHT)
        };
      }
    },
    ACTION_SELECT {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] {
            new MouseButtonTrigger(MouseInput.BUTTON_LEFT),
            new KeyTrigger(KeyInput.KEY_E)
        };
      }
    },
    ACTION_RELEASE {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] { new MouseButtonTrigger(MouseInput.BUTTON_RIGHT) };
      }
    },
    TOGGLE_MENU {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] { new KeyTrigger(KeyInput.KEY_ESCAPE) };
      }
    },
    DEBUG_PHYSICS {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] { new KeyTrigger(KeyInput.KEY_F9) };
      }
    },
    DEBUG_STATS {
      @Override
      public Trigger[] getTriggers() {
        return new Trigger[] { new KeyTrigger(KeyInput.KEY_F10) };
      }
    },
    ;

    public Trigger[] getTriggers() {
      return new Trigger[0];
    };
  }

  private boolean left, right, forward, backward, select, release;
  private boolean cursorVisibilityModifiable = true;
  private boolean flyCamActive = false;

  private long touchDownTime = NO_TOUCH_DOWN;
  private final Vector2f selectCoordinates = new Vector2f();

  private boolean debugPhysics;

  private Node cameraNode;
  private InputManager inputManager;
  private AppStateManager stateManager;

  private boolean invertY;
  private Vector3f initialUpVec = Vector3f.UNIT_Y;
  private float rotationSpeed = 1;

  @Override
  public void initialize(AppStateManager stateManager, Application app) {
    super.initialize(stateManager, app);

    this.cameraNode = WastelandApplication.getApplication().getCameraNode();
    this.inputManager = app.getInputManager();
    this.stateManager = stateManager;

    inputManager.setCursorVisible(false);

    inputManager.clearMappings();
    for (InputMappingName mappingName : InputMappingName.values()) {
      inputManager.addListener(this, mappingName.name());
      for (Trigger trigger : mappingName.getTriggers()) {
        inputManager.addMapping(mappingName.name(), trigger);
      }
    }

    // Forward all touch inputs
    inputManager.addMapping("TouchInput.ALL", new TouchTrigger(TouchInput.ALL));
    inputManager.addListener(this, "TouchInput.ALL");

    JmeCursor jmeCursor = (JmeCursor) app.getAssetManager().loadAsset(Assets.Interface.Cursor.cursor_selectable_ico);
    jmeCursor.setxHotSpot(jmeCursor.getWidth() / 2);
    jmeCursor.setyHotSpot(jmeCursor.getHeight() / 2);
    inputManager.setMouseCursor(jmeCursor);
  }

  @Override
  public void cleanup() {
    super.cleanup();

    inputManager.setMouseCursor(null);

    inputManager.setCursorVisible(true);
    inputManager.removeListener(this);
    inputManager.deleteMapping("TouchInput.ALL");
    for (InputMappingName mappingName : InputMappingName.values()) {
      inputManager.deleteMapping(mappingName.name());
    }

    this.cameraNode = null;
    this.inputManager = null;
    this.stateManager = null;
  }

  public boolean isCursorVisibilityModifiable() {
    return cursorVisibilityModifiable;
  }

  public void setCursorVisibilityModifiable(boolean cursorVisibilityModifiable) {
    this.cursorVisibilityModifiable = cursorVisibilityModifiable;
  }

  public boolean isCursorVisible() {
    return this.inputManager.isCursorVisible();
  }

  public void setCursorVisible(boolean cursorVisible) {
    this.inputManager.setCursorVisible(cursorVisible);
  }

  public boolean isMoving() {
    return isForward() || isBackward() || isLeft() || isRight();
  }

  public boolean isLeft() {
    return left;
  }

  public boolean isRight() {
    return right;
  }

  public boolean isForward() {
    return forward || (touchDownTime != NO_TOUCH_DOWN && System.currentTimeMillis() - touchDownTime > TOUCH_DOWN_THREASHOLD);
  }

  public boolean isBackward() {
    return backward;
  }

  public boolean consumeSelect() {
    boolean value = select;
    select = false;
    return value;
  }

  public boolean consumeRelease() {
    boolean value = release;
    release = false;
    return value;
  }

  public boolean isFlyCamActive() {
    return flyCamActive;
  }

  public float getSelectCoordinatesX() {
    return selectCoordinates.getX();
  }

  public float getSelectCoordinatesY() {
    return selectCoordinates.getY();
  }

  public void onAction(String binding, boolean value, float tpf) {
    InputMappingName inputMapping = InputMappingName.valueOf(binding);

    switch (inputMapping) {
    case MOVE_LEFT:
      left = value;
      release = value;
      break;
    case MOVE_RIGHT:
      right = value;
      release = value;
      break;
    case MOVE_FORWARD:
      forward = value;
      release = value;
      break;
    case MOVE_BACKWARD:
      backward = value;
      release = value;
      break;
    case ACTION_SELECT:
      select = value;
      if (value) {
        selectCoordinates.set(inputManager.getCursorPosition());
      }
      break;
    case ACTION_RELEASE:
      release = value;
      if (value) {
        if (cursorVisibilityModifiable) {
          inputManager.setCursorVisible(!inputManager.isCursorVisible());
        }
      }
      break;
    case DEBUG_PHYSICS:
      if (Launcher.isDevelopmentMode() &&
          value && stateManager.getState(BulletAppState.class) != null) {
        debugPhysics = !debugPhysics;
        stateManager.getState(BulletAppState.class).setDebugEnabled(debugPhysics);
      }
      break;
    case DEBUG_STATS:
      if (value && stateManager.getState(StatsAppState.class) != null) {
        stateManager.getState(StatsAppState.class).toggleStats();
      }
      break;
    case TOGGLE_MENU:
      if (value && stateManager.getState(MenuAppState.class) != null) {
        stateManager.getState(MenuAppState.class).toggleVisible();
        // TODO detach scene node
      }
      break;
    default:
      break;
    }
  }

  @Override
  public void onTouch(String name, TouchEvent event, float tpf) {
    switch (event.getType()) {
    case DOWN:
      touchDownTime = System.currentTimeMillis();
      break;
    case TAP:
      select = true;
      selectCoordinates.set(event.getX(), event.getDeltaY());
      break;
    case UP:
      touchDownTime = NO_TOUCH_DOWN;
      break;
    default:
      select = false;
      break;
    }
  }

  @Override
  public void onAnalog(String binding, float value, float tpf) {
    selectCoordinates.set(inputManager.getCursorPosition());

    InputMappingName inputMapping = InputMappingName.valueOf(binding);
    switch (inputMapping) {
    case ROTATE_UP:
      rotateCamera(-value * (invertY ? -1 : 1), cameraNode.getLocalRotation().getRotationColumn(0));
      break;
    case ROTATE_DOWN:
      rotateCamera(value * (invertY ? -1 : 1), cameraNode.getLocalRotation().getRotationColumn(0));
      break;
    case ROTATE_LEFT:
      rotateCamera(value, initialUpVec);
      break;
    case ROTATE_RIGHT:
      rotateCamera(-value, initialUpVec);
      break;
    default: // NOP
    }
  }

  protected void rotateCamera(float value, Vector3f axis) {
    if (inputManager.isCursorVisible()) {
      return;
    }

    TempVars tempVars = TempVars.get();

    Matrix3f mat = tempVars.tempMat3;
    mat.fromAngleNormalAxis(rotationSpeed * value, axis);

    Vector3f up = cameraNode.getLocalRotation().getRotationColumn(1);
    Vector3f left = cameraNode.getLocalRotation().getRotationColumn(0);
    Vector3f dir = cameraNode.getLocalRotation().getRotationColumn(2);

    mat.mult(up, up);
    mat.mult(left, left);
    mat.mult(dir, dir);

    Quaternion q = tempVars.quat1;
    q.fromAxes(left, up, dir);
    q.normalizeLocal();

    // Restric up & down
    float[] angles = q.toAngles(tempVars.fWdU);
    angles[0] = FastMath.clamp(angles[0], -FastMath.HALF_PI, FastMath.HALF_PI);
    q.fromAngles(angles);

    cameraNode.setLocalRotation(q);

    tempVars.release();
  }

}
