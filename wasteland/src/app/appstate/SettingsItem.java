/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app.appstate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.jme3.system.AppSettings;

public enum SettingsItem {

  DisplayMode {
    @Override
    public String getText(AppSettings settings) {
      return settings.getWidth() + "x" + settings.getHeight() + "@" + settings.getFrequency() + "Hz " + settings.getBitsPerPixel() + "bit";
    }

    @Override
    public void increaseValue(AppSettings settings) {
      incrementValue(settings, +1);
    }

    @Override
    public void decreaseValue(AppSettings settings) {
      incrementValue(settings, -1);
    }

    private void incrementValue(AppSettings settings, int increment) {
      List<DisplayMode> displayModes = getOrderedDisplayModes();
      if (!displayModes.isEmpty()) {
        int index = -1;
        for (int i = 0; i < displayModes.size(); i++) {
          DisplayMode mode = displayModes.get(i);
          if (mode.getWidth() == settings.getWidth() &&
              mode.getHeight() == settings.getHeight() &&
              mode.getFrequency() == settings.getFrequency() &&
              mode.getBitsPerPixel() == settings.getBitsPerPixel()
          ) {
            index = i;
            break;
          }
        }

        index = index + increment;
        if (index >= 0 && index < displayModes.size()) {
          DisplayMode newMode = displayModes.get(index);
          settings.setWidth(newMode.getWidth());
          settings.setHeight(newMode.getHeight());
          settings.setFrequency(newMode.getFrequency());
          settings.setBitsPerPixel(newMode.getBitsPerPixel());

          // Set the fullscreen to false if the selected mode is not fullscreen capable
          settings.setFullscreen(settings.isFullscreen() && newMode.isFullscreenCapable());
        }
      }
    }

  },
  VSync {
    @Override
    public String getText(AppSettings settings) {
      return settings.isVSync() ? "On" : "Off";
    }

    @Override
    public void increaseValue(AppSettings settings) {
      settings.setVSync(!settings.isVSync());
    }

    @Override
    public void decreaseValue(AppSettings settings) {
      settings.setVSync(!settings.isVSync());
    }
  },
  Fullscreen {
    @Override
    public String getText(AppSettings settings) {
      return settings.isFullscreen() ? "On" : "Off";
    }

    @Override
    public void increaseValue(AppSettings settings) {
      settings.setFullscreen(!settings.isFullscreen());
    }

    @Override
    public void decreaseValue(AppSettings settings) {
      settings.setFullscreen(!settings.isFullscreen());
    }
  },
  AntiAliasing {

    private final List<Integer> VALUES = Arrays.asList(0, 2, 4, 6, 8, 16);

    @Override
    public String getText(AppSettings settings) {
      int value = settings.getSamples();
      if (value == 0) {
        return "Off";
      }
      return settings.getSamples() + "x";
    }

    @Override
    public void increaseValue(AppSettings settings) {
      int value = incrementIntValue(VALUES, settings.getSamples(), +1);
      settings.setSamples(value);
    }

    @Override
    public void decreaseValue(AppSettings settings) {
      int value = incrementIntValue(VALUES, settings.getSamples(), -1);
      settings.setSamples(value);
    }

  },
  AnisotropicFiltering {

    // Actually OFF == 1 but the Texture class will fix that
    private final List<Integer> VALUES = Arrays.asList(0, 2, 4, 8, 16);

    @Override
    public String getText(AppSettings settings) {
      int value = settings.getInteger(name());
      if (value == 0) {
        return "Off";
      }
      return value + "x";
    }

    @Override
    public void increaseValue(AppSettings settings) {
      int value = incrementIntValue(VALUES, settings.getInteger(name()), +1);
      settings.putInteger(name(), value);
    }

    @Override
    public void decreaseValue(AppSettings settings) {
      int value = incrementIntValue(VALUES, settings.getInteger(name()), -1);
      settings.putInteger(name(), value);
    }
  };

  private static int incrementIntValue(List<Integer> values, int value, int increment) {
    int index = values.indexOf(value);
    index = index + increment;
    if (index >= 0 && index < values.size()) {
      value = values.get(index);
    }
    return value;
  }

  private static final Logger LOG = LoggerFactory.getLogger(MenuAppState.class);

  public abstract String getText(AppSettings settings);

  public abstract void increaseValue(AppSettings settings);

  public abstract void decreaseValue(AppSettings settings);

  public static List<DisplayMode> getOrderedDisplayModes() {
    List<Comparator<DisplayMode>> comparators = new ArrayList<>();
    comparators.add(new Comparator<DisplayMode>() {
      @Override
      public int compare(DisplayMode o1, DisplayMode o2) {
        return Integer.compare(o1.getWidth(), o2.getWidth());
      }
    });
    comparators.add(new Comparator<DisplayMode>() {
      @Override
      public int compare(DisplayMode o1, DisplayMode o2) {
        return Integer.compare(o1.getHeight(), o2.getHeight());
      }
    });
    comparators.add(new Comparator<DisplayMode>() {
      @Override
      public int compare(DisplayMode o1, DisplayMode o2) {
        return Integer.compare(o1.getFrequency(), o2.getFrequency());
      }
    });
    comparators.add(new Comparator<DisplayMode>() {
      @Override
      public int compare(DisplayMode o1, DisplayMode o2) {
        return Integer.compare(o1.getBitsPerPixel(), o2.getBitsPerPixel());
      }
    });

    List<DisplayMode> orderedModes = Collections.emptyList();
    try {
      orderedModes = Ordering.compound(comparators).sortedCopy(Arrays.asList(Display.getAvailableDisplayModes()));
    } catch (LWJGLException e) {
      LOG.error("", e);
    }

    Iterables.removeIf(orderedModes, new Predicate<DisplayMode>() {
      @Override
      public boolean apply(DisplayMode mode) {
        return mode.getWidth() < 1024;
      }
    });

    LOG.debug("Found {} modes: {}", orderedModes.size(), orderedModes);

    return orderedModes;
  }
}
