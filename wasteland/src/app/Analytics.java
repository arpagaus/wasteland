/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app;

import java.util.UUID;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.brsanthu.googleanalytics.DefaultRequest;
import com.brsanthu.googleanalytics.GoogleAnalytics;
import com.brsanthu.googleanalytics.GoogleAnalyticsConfig;
import com.brsanthu.googleanalytics.GoogleAnalyticsRequest;

public class Analytics {

  private static final Logger LOG = LoggerFactory.getLogger(Analytics.class);
  private static final String ANALYTICS_PREF_KEY = Launcher.PREF_PREFIX + "analytics";

  private static GoogleAnalytics analytics;
  private static String clientId;

  private static GoogleAnalytics getAnalytics() {
    if (analytics == null) {
      GoogleAnalyticsConfig config = new GoogleAnalyticsConfig();
      config.setDeriveSystemParameters(true);
      config.setGatherStats(false);
      DefaultRequest defaultRequest = new DefaultRequest("appview", "UA-47433405-1", "Wasteland", Launcher.APP_VERSION);
      defaultRequest.anonymizeIp(true);
      analytics = new GoogleAnalytics(config, defaultRequest);
    }
    return analytics;
  }

  private static String getClientUuid() {
    if (clientId == null) {
      if (Launcher.isDevelopmentMode()) {
        clientId = new UUID(0, 0).toString();
      }
      else {
        Preferences preferences = Preferences.userRoot().node(ANALYTICS_PREF_KEY);
        clientId = preferences.get("clientUuid", UUID.randomUUID().toString());
        preferences.put(ANALYTICS_PREF_KEY, clientId);
        try {
          preferences.flush();
        } catch (BackingStoreException e) {
          LOG.error("Failed to store clientId", e);
        }
      }
    }
    return clientId;
  }

  public static void postSync(GoogleAnalyticsRequest<?> request) {
    try {
      request.clientId(getClientUuid().toString());
      getAnalytics().post(request);
    } catch (Exception e) {
      LOG.error("Failed to send analytics request", e);
    }
  }

  public static void postAsync(GoogleAnalyticsRequest<?> request) {
    try {
      request.clientId(getClientUuid().toString());
      getAnalytics().postAsync(request);
    } catch (Exception e) {
      LOG.error("Failed to send analytics request", e);
    }
  }
}
