/*******************************************************************************
 * Copyright (C) Remo Arpagaus - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Remo Arpagaus <arpagaus.remo@gmail.com>, 2013 - 2014
 ******************************************************************************/
package app;

import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;

public class FastMathEx {

  public static float rotationDistance(Quaternion start, Quaternion end) {
    return 1f - FastMath.sqr(end.normalizeLocal().dot(start.normalizeLocal()));
  }

  /**
   * θ=cos−1(2⟨q1,q2⟩2−1)
   * 
   * @see http://math.stackexchange.com/questions/90081/quaternion-distance
   * @param a
   * @param b
   * @return
   */
  public static float rotationAngle(Quaternion a, Quaternion b) {
    return FastMath.pow(FastMath.cos(a.dot(b) * 2 - 1), -1);
  }
}
