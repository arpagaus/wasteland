
uniform sampler2D m_Texture;
varying vec2 texCoord;

uniform vec2 m_Resolution;
uniform float m_Radius ;
uniform float m_Softness;
uniform float m_Strength;

void main() {
    vec4 texColor = texture2D(m_Texture, texCoord);
    vec2 position = (gl_FragCoord.xy / m_Resolution.xy) - vec2(0.5);
    float len = length(position);
    float vignette = smoothstep(m_Radius, m_Radius-m_Softness, len);
    texColor.rgb = mix(texColor.rgb, texColor.rgb * vignette, m_Strength);
    gl_FragColor = texColor;
}
