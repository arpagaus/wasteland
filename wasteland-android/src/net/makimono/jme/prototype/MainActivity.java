package net.makimono.jme.prototype;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import app.PrototypeApplication;

import com.jme3.app.AndroidHarness;
import com.jme3.renderer.android.TextureUtil;
import com.jme3.system.android.AndroidConfigChooser.ConfigType;

public class MainActivity extends AndroidHarness {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		TextureUtil.ENABLE_COMPRESSION = false;

		eglConfigType = ConfigType.FASTEST;
		screenOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
		mouseEventsInvertX = true;
		mouseEventsInvertY = true;

		appClass = PrototypeApplication.class.getName();

		super.onCreate(savedInstanceState);
	}
}