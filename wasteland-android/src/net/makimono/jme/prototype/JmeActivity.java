package net.makimono.jme.prototype;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import app.PrototypeApplication;

import com.jme3.app.AndroidHarness;
import com.jme3.system.android.AndroidConfigChooser.ConfigType;

public class JmeActivity extends AndroidHarness {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		appClass = PrototypeApplication.class.getName();

		eglConfigType=ConfigType.BEST;
		screenOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
		mouseEventsInvertX = true;
		mouseEventsInvertY = true;

		super.onCreate(savedInstanceState);
	}
}
